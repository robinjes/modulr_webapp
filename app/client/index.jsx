/* Librairies */
import React from 'react';
import { render } from 'react-dom';
// Components
import App from 'components/environments/App';

// Render
render(
  <App />,
  document.getElementById('modulr_webapp')
);
