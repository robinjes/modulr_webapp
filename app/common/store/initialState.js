/* Initial states */
import { initialState as appInitState } from 'reducers/application';
import { initialState as colorsInitState } from 'reducers/colors';
import { initialState as framesInitState } from 'reducers/frames';
import { initialState as modulesInitState } from 'reducers/modules';
import { initialState as moldingsInitState } from 'reducers/moldings';
import { initialState as referencesInitState } from 'reducers/properties';
import { initialState as propertiesInitState } from 'reducers/references';

const initialState = {
  application: appInitState,
  colors: colorsInitState,
  frames: framesInitState,
  modules: modulesInitState,
  moldings: moldingsInitState,
  references: referencesInitState,
  properties: propertiesInitState
};

export default initialState;
