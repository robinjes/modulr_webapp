/* global __DEV_TOOLS__ */
import { createStore, compose, applyMiddleware } from 'redux';
import { persistStore, autoRehydrate } from 'redux-persist';
import { reduxReactRouter } from 'redux-router';
import { createHistory } from 'history';
import thunk from 'redux-thunk';
/* Reducers */
import combinedReducer from 'reducers';
/* Initial state */
import initialState from 'common/store/initialState';

// Here we have all our application stores

const storeEnhancers = [
    reduxReactRouter({ createHistory }),
    autoRehydrate()
  ],

  composeEnhancers =
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
    }) : compose,

  combinedCreateStore = composeEnhancers(...storeEnhancers)(createStore),
  finalCreateStore = applyMiddleware(thunk)(combinedCreateStore),

  configureStore = () => {
    const store = finalCreateStore(combinedReducer, initialState);

    if (module.hot) {
      // Enable Webpack hot module replacement for reducers
      module.hot.accept('../reducers', () => {
        /* eslint-disable import/newline-after-import */
        /* eslint-disable global-require */
        const nextRootReducer = require('../reducers/index');
        store.replaceReducer(nextRootReducer);
      });
    }

    persistStore(store);

    return store;
  };

export default configureStore;
