/* global File, FileReader, FileList, Blob  */
class FileManager {
  constructor(imgType) {

    this.fileReader = FileReader;

    this.reader = new FileReader();
    this.reader.onloadstart = () => {
      // this.reader.abort();  // Stops the current read operation.
    };
    this.reader.onloadend = () => {
      // console.log(this.reader.error.message);
    };
    this.file = null;
    this.imgType = imgType;
  }

  /**
  * Check if FileReader is available in the browser
  */
  hasFileReader() {
    return !!this.fileReader;
  }

  handleDrop(e, callback) {
    this.file = e.dataTransfer.files[0];
    this.handleFile(callback);
  }

  handleInput(e, callback) {
    this.file = e.target.files[0];
    this.handleFile(callback);
  }

  handleFile(callback) {
    if (this.file.type.match(this.imgType)) {
      this.reader.onload = (event) => {
        callback(event.target.result);
      };
      this.reader.readAsDataURL(this.file);
    } else {
      console.log('Only images (.png, .jpg...)');
    }
  }

}

export default FileManager;
