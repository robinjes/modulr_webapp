/* global navigator, URL, webkitURL, MediaStreamTrack, errBack  */
class CameraManager {

  constructor() {

    this.getUserMedia = navigator.getUserMedia ||
                          navigator.webkitGetUserMedia ||
                          navigator.mozGetUserMedia ||
                          navigator.msGetUserMedia;

    this.video = null;
    this.canvas = null;
    this.localMediaStream = null;

  }

  /**
  * Check if User Media is available in the browser
  */
  hasGetUserMedia() {
    return !!this.getUserMedia;
  }

  /**
  * Instanciate Canvas for snapshot
  * @param {DOM} canvasEl - Canvas DOM element.
  */
  initSnapshot(canvasEl) {
    this.canvas = canvasEl;
    this.ctx = canvasEl.getContext('2d');
    this.ctx.translate(canvasEl.width, 0);
    this.ctx.scale(-1, 1);
  }

  /**
  * Enable instanciated camera
  * @param {object} options - constraints object
  * {
  *   audio: false,
  *   audioSource: false,
  *   videoSource: true
  *}
  * @param {function} callback - Callback function
  */

  enableCamera(options, videoEl, callback) {
    this.video = videoEl;
    // Get access to the camera!
    if (navigator.mediaDevices && navigator.mediaDevices.getUserMedia) {
      // Not adding `{ audio: true }` since we only want video now
      navigator.mediaDevices.getUserMedia({ video: true }).then((stream) => {
        this.video.src = URL.createObjectURL(stream);
        this.localMediaStream = stream;
      });
    } else if (this.getUserMedia) {
      this.getUserMedia({ video: true }, (stream) => {
        this.video.src = stream;
        this.localMediaStream = stream;
      }, errBack);
    }

    const sourceSelected = (audioSource, videoSource) => {
      const constraints = {
        video: {
          optional: [{ sourceId: videoSource }]
        }
      };

      if (options.audio) {
        constraints.audio = {
          optional: [{ sourceId: audioSource }]
        };
      }

      navigator.getUserMedia(constraints, () => {
        this.video.play();
      }, (err) => {
        console.log('The following error occurred: ' + err.name); // eslint-disable-line no-console
      });
    };

    if (options.audioSource && options.videoSource) {
      sourceSelected(options.audioSource, options.videoSource);
    } else if ('mediaDevices' in navigator) {
      navigator.mediaDevices.enumerateDevices().then((devices) => {
        let audioSource = null,
          videoSource = null;

        devices.forEach((device) => {
          if (device.kind === 'audio') {
            audioSource = device.id;
          } else if (device.kind === 'video') {
            videoSource = device.id;
          }
        });

        sourceSelected(audioSource, videoSource);
      })
      .catch((error) => {
        console.log(`${error.name}: ${error.message}`); // eslint-disable-line no-console
      });
    } else {
      MediaStreamTrack.getSources((sources) => {
        let audioSource = null,
          videoSource = null;

        sources.forEach((source) => {
          if (source.kind === 'audio') {
            audioSource = source.id;
          } else if (source.kind === 'video') {
            videoSource = source.id;
          }
        });

        sourceSelected(audioSource, videoSource);
      });
    }

    if (typeof callback === 'function') {
      callback();
    }

  }

  /**
  * Disable instanciated camera
  * @param {function} callback - Callback function
  */
  disableCamera(callback) {
    if (this.localMediaStream.stop) {
      this.localMediaStream.stop();
    } else {
      if (this.localMediaStream.getVideoTracks) {
        for (const track of this.localMediaStream.getVideoTracks()) {
          track.stop();
        }
      }
      if (this.localMediaStream.getAudioTracks) {
        for (const track of this.localMediaStream.getAudioTracks()) {
          track.stop();
        }
      }
    }

    if (typeof callback === 'function') {
      callback();
    }
  }
}

export default CameraManager;
