/* global localStorage */
const getToken = () => (
    localStorage.getItem('token')
  ),
  setToken = (token) => {
    localStorage.setItem('token', token);
  },
  isLogged = () => {
    const token = getToken();
    if (token && token.length > 0) {
      return true;
    }
    return false;
  };

export { getToken, setToken, isLogged };
