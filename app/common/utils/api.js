// src: https://github.com/dorsha/login-modal-react-redux/blob/master/src/util/api.js
// Librairies
import fetch from 'isomorphic-fetch';
// Actions
import { loginRequired } from 'actions/publics';

const constants = {
    APP_JSON_HEADER: 'application/json',
    SAME_ORIGIN: 'same-origin'
  },
  HttpCodes = {
    OK: 200,
    UNAUTHORIZED: 401
  };

let store;

function checkStatus(response) {
  let quiet = this.quiet;
  if (quiet === undefined) {
    quiet = false;
  }
  if (response.status === HttpCodes.UNAUTHORIZED && !quiet) {
    const request = this.request,
      promise = new Promise((resolve, reject) => {
        request.resolve = resolve;
        request.reject = reject;
      });
    store.dispatch(loginRequired(request));
    return promise;
  }

  if (response.status < HttpCodes.OK || response.status >= 300) {
    const error = new Error(response.statusText);
    error.response = response;
    throw error;
  }

  return response;
}

function parseJSON(response) {
  if (!response || response.constructor.name !== 'Response') {
    return response;
  }

  if (response.statusText === 'No Content') {
    return '';
  }
  return response.json();
}

function postPut(...args) {
  const uri = args[0],
    method = args[1] || 'POST',
    data = args[2] || '',
    quiet = args[3];
  return fetch(uri)
  // .then(
  //   checkStatus.bind({ request: postPut.bind(undefined, uri, method, data, true), quiet })
  // )
  // .then(parseJSON);
  .then(checkStatus.bind({ request: postPut.bind(undefined, uri, method, data, true), quiet }))
  .catch((err) => {
    console.log('Not able access the server. Try again later or contact the administrator.');
    console.log(err);
  })
  .then(parseJSON);
}

// export function get(...args) {
//   const uri = args[0],
//     quiet = args[1];
//   return fetch(uri,
//     {
//       method: 'GET',
//       credentials: constants.SAME_ORIGIN,
//       headers: {
//         Accept: constants.APP_JSON_HEADER
//       }
//     })
//     .then(checkStatus.bind({ request: get.bind(undefined, uri, true), quiet }))
//     .then(parseJSON);
// }

/*
* @params {boolean} quiet - set to true if we dont want to check if login is required.
*/
export function post(uri, data = '', quiet = false) {
  return postPut(uri, 'POST', data, quiet);
}

// export function put(uri, data = '', quiet = false) {
//   return postPut(uri, 'PUT', data, quiet);
// }

export function setStore(s) {
  store = s;
}
