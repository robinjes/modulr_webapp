// Librairies
import { createSelector } from 'reselect';
// Actions
import { loginRequired, archiveSucceeded } from 'actions/publics';
// Selectors
const applicationSelector = (state) => state.application,
  loginRequiredSelector = createSelector(
    applicationSelector,
    (application) => ({
      loginRequired: application.loginRequired,
      retriesQueue: application.retriesQueue
    })
  );

export default loginRequiredSelector;
