import Immutable from 'immutable';
import createReducer from 'utils/createReducer';
// Utils
import { isLogged } from 'utils/userManager';

/* Initial state */
const logged = isLogged(),
  initialState = {
    modals: {
      saveAction: {
        enable: false
      },
      loginAction: {
        enable: false
      }
    },
    logged
  },

  /* Values app */
  application = {
    REQUEST_LOGIN: (state, action) => {
      console.log('REQUEST_LOGIN');
      return state;
    },
    LOGGED_IN: (state, action) => {
      // SAVE JWT TO COOKIE or LOCALSTORAGE
      // UPDATE STORE STATE AS LOGGED
      // DISPLAY SUCCESS NOTIF
      console.log('LOGGED_IN');
      console.log(action.response);
      return state;
    },
    OPEN_MODAL: (state, action) => {
      const mutatedModals = state.modals;
      mutatedModals[action.modalType].enable = true;
      return JSON.parse(JSON.stringify(Object.assign({}, state, {
        modals: mutatedModals
      })));
    },
    CLOSE_MODAL: (state, action) => {
      const mutatedModals = state.modals;
      mutatedModals[action.modalType].enable = false;
      return JSON.parse(JSON.stringify(Object.assign({}, state, {
        modals: mutatedModals
      })));
    },
    CLOSE_ALL_MODALS: (state, action) => {
      const mutatedModals = state.modals,
        len = Object.keys(mutatedModals).length,
        parseState = function parseState(counter) {
          const ref = Object.keys(mutatedModals)[counter];
          if (counter >= len) {
            return mutatedModals;
          }
          mutatedModals[ref].enable = false;
          return parseState(counter + 1);
        };
      return JSON.parse(JSON.stringify(Object.assign({}, state, {
        modals: parseState(0)
      })));
    }
  };

export default createReducer(initialState, application);
