import createReducer from 'utils/createReducer';

let frameId = 0;

/* Initial state */
const initialState = {
    wip: {
      valid: false
    },
    items: {
      '0kdoe44': {
        id: '0kdoe44',
        width: 0,
        height: 0,
        thumbSrc: '' // tumbnail overview on frame,
        // ,algo: 'tl_BLACK_t_2xFIO_4xPINK_tr_BLACK_r_4xPINK_2xYELLOW_br_BLACK_b_3xPURPLE'
      },
      '8ueueueu89': {
        id: '8ueueueu89',
        width: 0,
        height: 0,
        thumbSrc: '' // tumbnail overview on frame
      }
    },
    itemsIds: ['0kdoe44', '8ueueueu89']
  },
  frames = {
    // CREATE/GET FRAME
    PREPARE_FRAME: (state, action) => (
      Object.assign({}, state, {
        isFetching: true
      })
    ),
    BUILD_FRAME: (state = { isFetching: false }, action) => {
      // if frame already built
      if (state.wip.valid === true) {
        return state;
      }

      if (((typeof action.docWidth === 'undefined') &&
            (typeof action.docHeight === 'undefined')) ||
            ((typeof action.docWidth === 'undefined') ||
            (typeof action.docHeight === 'undefined'))) {
        const falsyWip = state.wip;
        falsyWip.valid = false;
        return Object.assign({}, state, {
          wip: falsyWip
        });
      }

      const mutatedWipFrame = state.wip || {},
        artworkWidth = action.docWidth,
        artworkHeight = action.docHeight,
        matTotalWidth = typeof action.hMat !== 'undefined' ? action.hMat * 2 : 0,
        matTotalHeight = typeof action.vMat !== 'undefined' ? action.vMat * 2 : 0,
        moduleRef = action.moduleRef,
        deckRef = action.deckRef,
        defaultColor = action.defaultColor,
      // no offset taken into consideration if there is mat.
        vTotalOffset = matTotalWidth <= 0 ? moduleRef.offset * 2 : 0,
        hTotalOffset = matTotalHeight <= 0 ? moduleRef.offset * 2 : 0,
      // calculate sizes (mat and decks included)
      // round it ???????????????????????????????????
        frameWidth = Math.round(artworkWidth + matTotalWidth + moduleRef.height - vTotalOffset),
        frameHeight = Math.round(artworkHeight + matTotalHeight + moduleRef.width - hTotalOffset),
      // calculate algo
        hModulesQty = (frameWidth - (deckRef.width * 2)) / moduleRef.width,
        vModulesQty = (frameHeight - (deckRef.width * 2)) / moduleRef.width,
        algo = {
          m: { x: hModulesQty, y: vModulesQty },
          tl: { algo: defaultColor.id }, // top-left angle
          t: { algo: hModulesQty + 'x' + defaultColor.id }, // top branch
          tr: { algo: defaultColor.id }, // top-right angle
          r: { algo: vModulesQty + 'x' + defaultColor.id }, // right branch
          br: { algo: defaultColor.id }, // bottom-right angle
          b: { algo: hModulesQty + 'x' + defaultColor.id }, // bottom branch
          bl: { algo: defaultColor.id }, // bottom-left angle
          l: { algo: vModulesQty + 'x' + defaultColor.id }, // left branch
        };

      mutatedWipFrame.algo = algo;
      mutatedWipFrame.width = frameWidth;
      mutatedWipFrame.height = frameHeight;
      // validate frame wih regex and not empty ?
      mutatedWipFrame.valid = true;
      frameId++;

      return Object.assign({}, state, {
        wip: mutatedWipFrame
      });
    },
    RESET_WIP: (state, action) => {
      const mutatedWipFrame = state.wip || {};
      mutatedWipFrame.valid = false;
      return Object.assign({}, state, {
        wip: mutatedWipFrame
      });
    },
    // SAVE FRAME
    REQUEST_SAVE_FRAME: (state, action) => (state),
    FRAME_SAVED: (state, action) => {
      const wip = state.wip || {},
        message = { key: 'FRAME_SAVED', status: action.status };
      wip.id = action.id;

      return Object.assign({}, state, {
        wip,
        isFetching: false,
        message
      });
    },
    // GET FRAME
    REQUEST_FRAME: (state, action) => (
      Object.assign({}, state, {
        isFetching: false
      })
    ),
    LOAD_FRAME: (state, action) => (
      Object.assign({}, state, {
        wip: action.res
      })
    ),
    RECEIVE_FRAME: (state, action) => (
      Object.assign({}, state, {
        isFetching: false
      })
    ),
    // GET PUBLIC FRAME
    REQUEST_PUBLIC_FRAME: (state, action) => (
      Object.assign({}, state, {
        isFetching: true
      })
    ),
    LOAD_PUBLIC_FRAME: (state, action) => (
      Object.assign({}, state, {
        wip: action.res
      })
    ),
    RECEIVE_PUBLIC_FRAME: (state, action) => (
      Object.assign({}, state, {
        isFetching: false
      })
    ),
    // GET 3D FRAME
    REQUEST_FRAME_3D: (state, action) => (
      Object.assign({}, state, {
        isFetching: true
      })
    ),
    LOAD_FRAME_3D: (state, action) => {
      const mutatedWipFrame = state.wip || {};
      mutatedWipFrame.mesh = action.mesh;
      return Object.assign({}, state, {
        wip: mutatedWipFrame
      });
    },
    RECEIVE_FRAME_3D: (state, action) => (
      Object.assign({}, state, {
        isFetching: false
      })
    ),
    // GET ALL FRAMES
    REQUEST_ALL_FRAMES: (state, action) => (
      Object.assign({}, state, {
        isFetching: true
      })
    ),
    LOAD_ALL_FRAMES: (state, action) => {
      const items = action.res,
        itemsIds = items.map(item => item.id);
      return Object.assign({}, state, {
        items,
        itemsIds
      });
    },
    RECEIVE_ALL_FRAMES: (state, action) => (
      Object.assign({}, state, {
        isFetching: false
      })
    ),
    // GET ALL FRAMES OVERVIEWS
    REQUEST_ALL_FRAMES_OVERVIEWS: (state, action) => (
      Object.assign({}, state, {
        isFetching: true
      })
    ),
    LOAD_ALL_FRAMES_OVERVIEWS: (state, action) => {
      const items = action.res,
        itemsIds = items.map(item => item.id);
      return Object.assign({}, state, {
        items,
        itemsIds
      });
    },
    RECEIVE_ALL_FRAMES_OVERVIEWS: (state, action) => (
      Object.assign({}, state, {
        isFetching: false
      })
    ),
    // GET ALL PUBLIC FRAMES
    REQUEST_ALL_PUBLIC_FRAMES: (state, action) => (
      Object.assign({}, state, {
        isFetching: true
      })
    ),
    LOAD_ALL_PUBLIC_FRAMES: (state, action) => {
      const items = action.res,
        itemsIds = items.map(item => item.id);
      return Object.assign({}, state, {
        items,
        itemsIds
      });
    },
    RECEIVE_ALL_PUBLIC_FRAMES: (state, action) => (
      Object.assign({}, state, {
        isFetching: false
      })
    ),
    // GET ALL PUBLIC FRAMES OVERVIEWS
    REQUEST_AL_PUBLICL_FRAMES_OVERVIEWS: (state, action) => (
      Object.assign({}, state, {
        isFetching: true
      })
    ),
    LOAD_ALL_PUBLIC_FRAMES_OVERVIEWS: (state, action) => {
      const items = action.res,
        itemsIds = items.map(item => item.id);
      return Object.assign({}, state, {
        items,
        itemsIds
      });
    },
    RECEIVE_ALL_PUBLIC_FRAMES_OVERVIEWS: (state, action) => (
      Object.assign({}, state, {
        isFetching: false
      })
    ),
    UPDATE_FRAME_NAME: (state, action) => {
      const mutatedWipFrame = state.wip || {};
      mutatedWipFrame.name = action.name;
      return JSON.parse(JSON.stringify(Object.assign({}, state, {
        wip: mutatedWipFrame
      })));
    },
    UPDATE_FRAME_STATUS: (state, action) => {
      const mutatedWipFrame = state.wip || {};
      mutatedWipFrame.isPublic = action.status;
      return JSON.parse(JSON.stringify(Object.assign({}, state, {
        wip: mutatedWipFrame
      })));
    }
  };

export default createReducer(initialState, frames);
