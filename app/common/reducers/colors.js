import createReducer from 'utils/createReducer';

/* Initial state */
const initialState = {
    selectedId: '99-855',
    items: {
      BLACK: {
        id: 'BLACK',
        name: 'Noir',
        rgba: [0, 0, 0, 1],
        finition: 'satin',
        texture: null
      },
      WHITE: {
        id: 'WHITE',
        name: 'Blanc',
        rgba: [255, 255, 255, 1],
        finition: 'satin',
        texture: null
      },
      'golden-leaf': {
        id: 'golden-leaf',
        name: 'Doré',
        rgba: null,
        finition: 'satin',
        texture: 'leaf'
      },
      'silvered-leaf': {
        id: 'silvered-leaf',
        name: 'Argenté',
        rgba: null,
        finition: 'satin',
        texture: 'leaf'
      },
      '99-855': {
        id: '99-855',
        name: '99-855',
        rgba: [87, 90, 91, 1],
        finition: 'satin',
        texture: null
      },
      '22-073': {
        id: '22-073',
        name: '22-073',
        rgba: [103, 96, 88, 1],
        finition: 'satin',
        texture: null
      },
      '50-062': {
        id: '50-062',
        name: '50-062',
        rgba: [122, 121, 109, 1],
        finition: 'satin',
        texture: null
      },
      '18-224': {
        id: '18-224',
        name: '18-224',
        rgba: [101, 81, 63, 1],
        finition: 'satin',
        texture: null
      },
      '89-050': {
        id: '89-050',
        name: '89-050',
        rgba: [107, 81, 95, 1],
        finition: 'satin',
        texture: null
      },
      '04-254': {
        id: '04-254',
        name: '04-254',
        rgba: [138, 73, 79, 1],
        finition: 'satin',
        texture: null
      },
      '08-164': {
        id: '08-164',
        name: '08-164',
        rgba: [130, 91, 85, 1],
        finition: 'satin',
        texture: null
      },
      '10-245': {
        id: '10-245',
        name: '10-245',
        rgba: [173, 115, 92, 1],
        finition: 'satin',
        texture: null
      },
      '08-166': {
        id: '08-166',
        name: '08-166',
        rgba: [184, 142, 130, 1],
        finition: 'satin',
        texture: null
      },
      '98-017': {
        id: '98-017',
        name: '98-017',
        rgba: [190, 76, 96, 1],
        finition: 'satin',
        texture: null
      },
      '07-445': {
        id: '07-445',
        name: '07-445',
        rgba: [214, 82, 77, 1],
        finition: 'satin',
        texture: null
      },
      '22-444': {
        id: '22-444',
        name: '22-444',
        rgba: [213, 172, 126, 1],
        finition: 'satin',
        texture: null
      },
      '10-626': {
        id: '10-626',
        name: '10-626',
        rgba: [254, 128, 35, 1],
        finition: 'satin',
        texture: null
      },
      '26-499': {
        id: '26-499',
        name: '26-499',
        rgba: [255, 193, 0, 1],
        finition: 'satin',
        texture: null
      },
      '28-499': {
        id: '28-499',
        name: '28-499',
        rgba: [237, 204, 102, 1],
        finition: 'satin',
        texture: null
      },
      '47-405': {
        id: '47-405',
        name: '47-405',
        rgba: [94, 137, 66, 1],
        finition: 'satin',
        texture: null
      },
      '47-162': {
        id: '47-162',
        name: '47-162',
        rgba: [108, 121, 90, 1],
        finition: 'satin',
        texture: null
      },
      '55-253': {
        id: '55-253',
        name: '55-253',
        rgba: [70, 101, 81, 1],
        finition: 'satin',
        texture: null
      },
      '61-253': {
        id: '61-253',
        name: '61-253',
        rgba: [31, 90, 90, 1],
        finition: 'satin',
        texture: null
      },
      '78-253': {
        id: '78-253',
        name: '78-253',
        rgba: [57, 76, 116, 1],
        finition: 'satin',
        texture: null
      },
      '75-123': {
        id: '75-123',
        name: '75-123',
        rgba: [65, 92, 117, 1],
        finition: 'satin',
        texture: null
      },
      '78-484': {
        id: '78-484',
        name: '78-484',
        rgba: [38, 109, 164, 1],
        finition: 'satin',
        texture: null
      },
      '75-225': {
        id: '75-225',
        name: '75-225',
        rgba: [70, 149, 186, 1],
        finition: 'satin',
        texture: null
      },
      '88-241': {
        id: '88-241',
        name: '88-241',
        rgba: [182, 131, 145, 1],
        finition: 'satin',
        texture: null
      },
      '83-335': {
        id: '83-335',
        name: '83-335',
        rgba: [124, 90, 132, 1],
        finition: 'satin',
        texture: null
      },
      '98-021': {
        id: '98-021',
        name: '98-021',
        rgba: [211, 196, 174, 1],
        finition: 'satin',
        texture: null
      }
    },
    itemIds: [
      'BLACK', 'WHITE',
      'golden-leaf', 'silvered-leaf',
      '99-855', '22-073', '50-062', '18-224', '89-050',
      '04-254', '08-164', '10-245', '08-166', '98-017',
      '07-445', '22-444', '10-626', '26-499', '28-499',
      '47-405', '47-162', '55-253', '61-253', '78-253',
      '75-123', '78-484', '75-225', '88-241', '83-335',
      '98-021'
    ]
  },

  /* Color reducer */
  color = (state = {}, action) => {
    switch (action.type) {

      case 'SELECT_COLOR':
        if (state.id !== action.id) {
          return Object.assign({}, state, {
            selected: false
          });
        }
        return Object.assign({}, state, {
          selected: true
        });

      default:
        return state;
    }
  },
  /* Colors items reducer */
  colorItems = (state = [], action) => {

    /* iteration function - start */
    const newState = {},
      iterate = function iterate(counter, nLoops) {
        if (counter >= nLoops) {
          return newState;
        }
        newState[Object.keys(state)[counter]] = color(state[Object.keys(state)[counter]], action);
        return iterate(counter + 1, nLoops);
      };
    /* iteration function - end */

    switch (action.type) {
      case 'SELECT_COLOR': {
        return iterate(0, Object.keys(state).length);
      }

      default:
        return state;
    }
  },
  /* Colors slice reducer */
  colors = {
    SELECT_COLOR: (state, action) => (
      Object.assign({}, state, {
        selectedId: action.id,
        items: colorItems(state.items, action)
      })
    ),
    // GET FINITIONS
    REQUEST_FINITIONS: (state, action) => (
      Object.assign({}, state, {
        isFetching: true
      })
    ),
    LOAD_FINITIONS: (state, action) => {
      const items = action.res,
        itemsIds = items.map(item => item.id);
      return Object.assign({}, state, {
        items,
        itemsIds
      });
    },
    RECEIVE_FINITIONS: (state, action) => (
      Object.assign({}, state, {
        isFetching: false
      })
    )
  };

export default createReducer(initialState, colors);
