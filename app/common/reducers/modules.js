import createReducer from 'utils/createReducer';

/* Initial state */
const initialState = [],

  /* Module reducer */
  module = (state = {}, action) => {
    switch (action.type) {
      case 'ADD_MODULE':
        return {
          id: action.id,
          size: action.size
        };

      case 'CHANGE_MODULE_COLOR':
        if (String(state.id) !== String(action.id)) {
          return state;
        }

        return Object.assign({}, state, {
          colorRef: action.colorRef
        });

      case 'TOGGLE_MODULE_SELECT':
        if (state.id !== action.id) {
          return state;
        }
        return Object.assign({}, state, {
          selected: !state.selected
        });

      default:
        return state;
    }
  },
  /* Modules items reducer */
  items = (state = [], action) => {
    switch (action.type) {
      case 'ADD_MODULE':
        return [
          ...state,
          module(undefined, action)
        ];

      case 'CHANGE_MODULE_COLOR': {
        const len = Object.keys(state).length,
          newState = {},
          parseState = function parseState(counter) {
            const ref = Object.keys(state)[counter];
            if ((counter >= len) || (ref === 'm')) {
              return newState;
            }
            newState[ref] = state[ref].map(t =>
              module(t, action)
            );
            return parseState(counter + 1);
          };
        return parseState(0);
      }

      case 'TOGGLE_MODULE_SELECT': {
        const len = Object.keys(state).length,
          newState = {},
          parseState = function parseState(counter) {
            const ref = Object.keys(state)[counter];
            if (counter >= len) {
              return newState;
            }
            newState[ref] = state[ref].map(t =>
              module(t, action)
            );
            return parseState(counter + 1);
          };
        return parseState(0);
      }

      default:
        return state;
    }
  },
  /* Modules slice reducer */
  modules = {
    REQUEST_MODULES: (state, action) => (
      Object.assign({}, state, {
        isFetching: true
      })
    ),
    LOAD_MODULES: (state, action) => {
      // if modules already built
      if (state.valid === true) {
        return state;
      }

      if ((typeof action.algo === 'undefined') || (action.algo === null)) {
        return Object.assign({}, state, {
          items: [],
          valid: false
        });
      }

      let modulesCounter = 0;

      const algos = action.algo,  // {0: { algo: defaultColor.id }, 1: { algo: hMo...}
        modulesArrs = {},
        splitRgx = '_',
        convertAlgoToModules = function convertAlgoToModules(sideRef) {
          const algo = algos[sideRef].algo,   // 'EEE_4xPINK_5xJOO'
            splitAlgo = algo.split(splitRgx), // ["EEE", "4xPINK", "5xJOO"]
            splitAlgoLen = splitAlgo.length,  // 3
            itemsArr = [];

          function level2(counterL2) {
            if (counterL2 >= splitAlgoLen) {
              return itemsArr;
            }
            const algoPart = splitAlgo[counterL2], // "EEE" or "4xPINK"
              containsX = algoPart.indexOf('x') >= 0,
              colorRef = containsX ? algoPart.split('x')[1] : algoPart;  // 'EEE' or 'PINK'

            let algoPartQty = containsX ? algoPart.substring(0, algoPart.indexOf('x')) : 1;
            // '1' or '4'
            algoPartQty = parseFloat(algoPartQty); // 1 or 4

            function level1(counterL1) {
              if (counterL1 >= algoPartQty) {
                return level2(counterL2 + 1);
              }
              const moduleEl = { id: modulesCounter, colorRef, selected: false };
              itemsArr.push(moduleEl);
              modulesCounter++;
              return level1(counterL1 + 1);
            }

            return level1(0);

          }

          return level2(0);

        };

      modulesArrs.tl = convertAlgoToModules('tl');
      modulesArrs.t = convertAlgoToModules('t');
      modulesArrs.tr = convertAlgoToModules('tr');
      modulesArrs.r = convertAlgoToModules('r');
      modulesArrs.br = convertAlgoToModules('br');
      modulesArrs.b = convertAlgoToModules('b');
      modulesArrs.bl = convertAlgoToModules('bl');
      modulesArrs.l = convertAlgoToModules('l');

      return Object.assign({}, state, {
        m: { x: algos.m.x, y: algos.m.y },
        items: modulesArrs,
        valid: true
      });
    },
    RECEIVE_MODULES: (state, action) => (
      Object.assign({}, state, {
        isFetching: false
      })
    ),
    ADD_MODULE: (state, action) => (
      Object.assign({}, state, {
        items: items(state.items, action)
      })
    ),
    CHANGE_MODULE_COLOR: (state, action) => (
      Object.assign({}, state, {
        items: items(state.items, action)
      })
    ),
    TOGGLE_MODULE_SELECT: (state, action) => (
      Object.assign({}, state, {
        items: items(state.items, action)
      })
    ),
    RESET_MODULES: (state, action) => (
      Object.assign({}, state, {
        items: {},
        valid: false
      })
    )
  };

export default createReducer(initialState, modules);
