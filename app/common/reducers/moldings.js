import createReducer from 'utils/createReducer';

/* Initial state */
const initialState = {
    isFetching: false
  },

/* Color reducer */
  molding = (state = {}, action) => {
    switch (action.type) {

      /* case 'SELECT_MOLDING':
        if (state.id !== action.id) {
          return Object.assign({}, state, {
            selected: false
          });
        }
        return Object.assign({}, state, {
          selected: true
        });*/

      default:
        return state;
    }
  },
  /* moldings items reducer */
  items = (state = [], action) => {

    /* iteration function - start */
    /* const newState = {},
      iterate = function iterate(counter, nLoops) {
        if (counter >= nLoops) {
          return newState;
        }
        newState[Object.keys(state)[counter]] = molding(state[Object.keys(state)[counter]], action);
        return iterate(counter + 1, nLoops);
      }; */
    /* iteration function - end */

    switch (action.type) {
      /* case 'SELECT_MOLDING': {
        return iterate(0, Object.keys(state).length);
      }*/

      default:
        return state;
    }
  },
  /* moldings slice reducer */
  moldings = {
    // GET MOLDINGS
    REQUEST_MOLDINGS: (state, action) => (
      Object.assign({}, state, {
        isFetching: true
      })
    ),
    LOAD_MOLDINGS: (state, action) => {
      const itemsRes = action.res,
        itemsIds = items.map(item => item.id);
      return Object.assign({}, state, {
        items: itemsRes,
        itemsIds
      });
    },
    RECEIVE_MOLDINGS: (state, action) => (
      Object.assign({}, state, {
        isFetching: false
      })
    )
  };

export default createReducer(initialState, moldings);
