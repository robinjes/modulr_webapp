import createReducer from 'utils/createReducer';

let newState = {};

/* Initial state */
const initialState = {
    artworkWidthProp: {
      name: 'artwork_width',
      label: 'Width',
      placeholder: '... artwork width',
      regex: { min: 10, max: 80, type: 'number' }
    },
    artworkHeightProp: {
      name: 'artwork_height',
      label: 'Height',
      placeholder: '... artwork height',
      regex: { min: 10, max: 80, type: 'number' }
    },
    artworkTypeProp: {
      name: 'artwork_type',
      label: 'Type',
      placeholder: 'Choisissez le genre de document...',
      regex: { min: 1, type: 'select' },
      savedOption: 0,
      options: [
        { id: 0, label: 'Toile sur chassis' },
        { id: 1, label: 'Document (Photo, Certificat ...)' },
        { id: 2, label: 'Autre' }
      ]
    },
    matSizeProp: {
      name: 'mat_size',
      value: {
        horiSizeProp: {
          customName: 'horizontal',
          customValue: 0,
          regex: { min: 0, max: 5, type: 'customNumber' }
        },
        vertiSizeProp: {
          customName: 'vertical',
          customValue: 0,
          regex: { min: 0, max: 5, type: 'customNumber' }
        }
      },
      label: 'Size',
      placeholder: '... taille du passe-partout',
      regex: { type: 'customNumber', enable: false }
    }
  },

  /* Values reducer */
  property = (state = {}, action) => {
    switch (action.type) {
      case 'VALIDATE_VALUE':

        if (state.name !== action.name) {
          return state;
        }

        // Input validation
        if (state.regex.type === 'number') {
          if (
            (action.value >= state.regex.min) &&
            (action.value <= state.regex.max)
          ) {
            // InputNum validation
            return Object.assign({}, state, {
              valid: true,
              value: action.value
            });
          }
        }

        if (state.regex.type === 'customNumber') {
          const actionValue = action.value,
            stateValue = state.value;
          let result = true;
          for (let i = 0; i < actionValue.length; i++) {
            result = (actionValue[Object.keys(actionValue)[i]].customValue
                      >= stateValue[i].regex.min)
                      ? result
                      : false;
            result = (actionValue[Object.keys(actionValue)[i]].customValue
                      <= stateValue[Object.keys(stateValue)[i]].regex.max)
                      ? result
                      : false;
          }
          if (result === true) {
            // InputNum validation
            return Object.assign({}, state, {
              valid: true,
              value: action.value
            });
          }
        }

        if (state.regex.type === 'select') {
          // InputSelect validation
          if (action.regex.selectedIndex >= state.regex.min) {
            return Object.assign({}, state, {
              valid: true,
              savedOption: action.regex.selectedIndex - 1
            });
          }
        }

        if (state.regex.type === 'text') {
          if (
            (action.value.length >= state.regex.min) &&
            (action.value.length <= state.regex.max)
          ) {
            return Object.assign({}, state, {
              valid: true,
              value: action.value
            });
          }
        }

        return Object.assign({}, state, {
          valid: false
        });

      case 'RESET_CUSTOM': {
        if (state.name !== action.name) {
          return state;
        }
        const mutatedValue = state.value,
          mutatedRegex = state.regex;
        mutatedRegex.enable = false;
        for (let i = 0; i < mutatedValue.length; i++) {
          mutatedValue[Object.keys(mutatedValue)[i]].customValue = 0;
        }
        return Object.assign({}, state, {
          value: mutatedValue,
          regex: mutatedRegex
        });
      }

      case 'SAVE_CUSTOM': {
        if (state.name !== action.name) {
          return state;
        }
        const mutatedRegex = state.regex;
        mutatedRegex.enable = true;
        return Object.assign({}, state, {
          value: action.value,
          regex: mutatedRegex
        });
      }

      case 'UPDATE_REGEX': {
        if (state.name !== action.name) {
          return state;
        }
        const mutatedRegex = state.regex;
        mutatedRegex[action.regexName] = action.regexValue;
        return Object.assign({}, state, {
          regex: mutatedRegex
        });
      }

      case 'UPDATE_CUSTOM_REGEX': {
        if (state.name !== action.name) {
          return state;
        }
        const mutatedValue = state.value;
        for (let i = 0; i < mutatedValue.length; i++) {
          if (mutatedValue[Object.keys(mutatedValue)[i]].customName === action.customName) {
            mutatedValue[Object.keys(mutatedValue)[i]].regex[action.regexName] = action.regexValue;
          }
        }
        return Object.assign({}, state, {
          value: mutatedValue
        });
      }

      default:
        return state;
    }
  },

  /* iteration function - start */
  iterate = function iterate(nLoops, state, action) {
    if (nLoops === -1) {
      return newState;
    }
    const newValue = property(state[Object.keys(state)[nLoops]], action);
    newState[Object.keys(state)[nLoops]] = newValue;
    return iterate(nLoops - 1, state, action);
  },
  /* iteration function - end */

  properties = {
    VALIDATE_VALUE: (state, action) => {
      newState = {};
      return iterate(Object.keys(state).length - 1, state, action);
    },
    RESET_CUSTOM: (state, action) => {
      newState = {};
      return iterate(Object.keys(state).length - 1, state, action);
    },
    SAVE_CUSTOM: (state, action) => {
      newState = {};
      return iterate(Object.keys(state).length - 1, state, action);
    },
    UPDATE_REGEX: (state, action) => {
      newState = {};
      return iterate(Object.keys(state).length - 1, state, action);
    },
    UPDATE_CUSTOM_REGEX: (state, action) => {
      newState = {};
      return iterate(Object.keys(state).length - 1, state, action);
    }
  };

export default createReducer(initialState, properties);
