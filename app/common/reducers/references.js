import createReducer from 'utils/createReducer';

/* Initial state */
const initialState = {
    moduleRef: {
      name: 'module_ref',
      label: 'Width',
      regex: { width: 1, height: 10, offset: 1, type: 'number' }
    },
    deckRef: {
      id: 3,
      name: 'deck_ref',
      label: 'Deck',
      regex: { width: 14.5, type: 'number' }
    },
    frameRef: {
      name: 'frame_ref',
      label: 'Width',
      regex: {
        width: { min: 33, max: 100, type: 'number' },
        height: { min: 33, max: 100, type: 'number' }
      }
    }
  },

  /* Values reducer */
  reference = (state = {}, action) => {
    switch (action.type) {
      default:
        return state;
    }
  },

  references = {};

export default createReducer(initialState, references);
