// Librairies
import React, { PropTypes } from 'react';
// Styles
require('./styles');
// Render
const Video = (props) => {

  const { className, id, width, height, autoplay } = props,
    classCtn = className ? 'video ' + className : 'video';

  return (
    <video className={classCtn} {...props} id={id} width={width} height={height} autoPlay />
  );

};

Video.propTypes = {
  className: PropTypes.string,
  id: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  autoplay: PropTypes.bool
};

export default Video;
