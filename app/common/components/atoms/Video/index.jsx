// Librairies
import React, { PropTypes } from 'react';
// Components
import Video from './Video';
// Render
class VideoCtn extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Video {...this.props} />
    );
  }

}

VideoCtn.propTypes = {
  className: PropTypes.string,
  id: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number,
  autoplay: PropTypes.bool
};

export default VideoCtn;
