// Librairies
import React, { PropTypes } from 'react';
// Styles
require('./styles');
// Render
const InputSelect = (props) => {

  const classCtn = props.className ? 'inputNum ' + props.className : 'inputNum';

  return (
    <div>
      <label htmlFor={props.elId}>{props.label}</label>
      <select
        id={props.elId}
        name={props.name}
        className={classCtn}
        onBlur={props.onBlur}
        onChange={props.onBlur}
      >
        <option>{props.placeholder}</option>
        {props.options.map(value =>
          <option key={value.id}>{value.label}</option>
        )}
      </select>
      <span className={!props.valid ? 'inputField_error' : 'inputField_info'}>
        {'Select a value'}
      </span>
    </div>
  );

};

InputSelect.propTypes = {
  className: PropTypes.string,
  elId: PropTypes.string,
  placeholder: PropTypes.string,
  label: PropTypes.string,
  regex: PropTypes.object,
  valid: PropTypes.bool,
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
  onCustomClick: PropTypes.func,
  name: React.PropTypes.string,
  options: React.PropTypes.array
};

InputSelect.defaultProps = {
  valid: true
};

export default InputSelect;
