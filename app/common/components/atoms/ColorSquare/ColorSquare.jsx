// Librairies
import React, { PropTypes } from 'react';
// Styles
require('./styles');
// Render
const ColorSquare = (props) => {

  const { onClick, selected, hovered, color, className } = props;
  /* Built-in style */
  let colorStyle = null,
  /* Built-in class */
    classCtn = className ? 'colorSquare ' + className : 'colorSquare';

  classCtn += selected ? ' selected' : '';
  classCtn += hovered ? ' hovered' : '';

  if (color.rgba && !hovered) {
    colorStyle = {
      backgroundColor: 'rgba(' + String(color.rgba[0])
                              + ', ' + String(color.rgba[1]) + ', '
                              + String(color.rgba[2]) + ', '
                              + String(color.rgba[3]) + ')'
    };
  }

  return (
    <div
      style={colorStyle}
      className={classCtn}
      onMouseEnter={props.onMouseEnter}
      onMouseLeave={props.onMouseLeave}
      onClick={props.onClick}
      data-paramfirst={color.id}

    />
  );

};

ColorSquare.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func,
  onMouseEnter: PropTypes.func,
  onMouseLeave: PropTypes.func,
  selected: PropTypes.bool,
  hovered: PropTypes.bool,
  color: PropTypes.object.isRequired
};

export default ColorSquare;
