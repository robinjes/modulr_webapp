// Librairies
import React, { PropTypes } from 'react';
// Components
import ColorSquare from './ColorSquare';
// Render
class ColorSquareCtn extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isHovered: false
    };

    this.handleMouseClick = this.handleMouseClick.bind(this);
    this.handleMouseEnter = this.handleMouseEnter.bind(this);
    this.handleMouseLeave = this.handleMouseLeave.bind(this);

  }

  handleMouseEnter() {
    this.setState({ isHovered: true });
  }

  handleMouseLeave() {
    this.setState({ isHovered: false });
  }

  handleMouseClick(evt) {
    const element = evt.target,
      colorRef = element.dataset.paramfirst;

    this.props.onColorClick(colorRef);
  }

  render() {

    return (
      <ColorSquare
        {...this.props}
        selected={this.props.color.selected}
        onClick={this.handleMouseClick}
        onMouseEnter={this.handleMouseEnter}
        onMouseLeave={this.handleMouseLeave}
        hovered={this.state.isHovered}
      />
    );
  }

}

ColorSquareCtn.propTypes = {
  onColorClick: PropTypes.func,
  selected: PropTypes.bool,
  color: PropTypes.object.isRequired
};

export default ColorSquareCtn;
