// Librairies
import React, { PropTypes } from 'react';
// Styles
require('./styles');
// Render
const Loader = (props) => {
  const { className, children, type, active } = props;
  let classCtn = className ? 'loader ' + className : 'loader';
  classCtn += active ? ' active' : '';

  return (
    <div>
      <div
        className={'veil' + (active ? ' active' : '') + (type ? ' ' + type : '')}
      />
      <div className={classCtn}>
        <div className="b1" />
        <div className="b2" />
        {children}
      </div>
    </div>
  );

};

Loader.propTypes = {
  className: PropTypes.string,
  children: PropTypes.string,
  type: PropTypes.string,
  active: PropTypes.bool
};

Loader.defaultProps = {
  type: 'outer',
  active: false
};

export default Loader;
