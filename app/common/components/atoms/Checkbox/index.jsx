// Librairies
import React, { PropTypes } from 'react';
// Styles
require('./styles');
// Render
const Checkbox = ({ elId, className, name, label, checked, onChange }) => {

  const classCtn = className ? 'checkbox ' + className : 'checkbox';

  return (
    <div className={classCtn}>
      <input
        type="checkbox"
        name={name}
        id={elId}
        onChange={onChange}
        checked={checked}
      />
      <label htmlFor={elId}>{label}</label>
    </div>
  );

};

Checkbox.propTypes = {
  elId: PropTypes.string,
  className: PropTypes.string,
  name: PropTypes.string,
  label: PropTypes.string,
  checked: PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.bool
  ]),
  onChange: PropTypes.func
};

Checkbox.defaultProps = {
  checked: false
};

export default Checkbox;
