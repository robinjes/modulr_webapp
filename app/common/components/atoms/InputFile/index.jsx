// Librairies
import React, { PropTypes } from 'react';
// Styles
require('./styles');

// Render
const InputFile = (props) => {

  let classCtn = props.className ? 'inputFile ' + props.className : 'inputFile';
  classCtn += props.fullZone ? ' fullZone' : '';

  return (
    <input
      {...props}
      className={classCtn}
      type="file"
      name={props.name}
      onChange={props.onChange}
    />
  );

};

InputFile.propTypes = {
  className: PropTypes.string,
  name: PropTypes.string,
  fullZone: PropTypes.bool,
  onChange: PropTypes.func
};

export default InputFile;
