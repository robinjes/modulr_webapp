// Librairies
import React, { PropTypes } from 'react';
// Components
import Button from 'components/atoms/Button';
// Styles
require('./styles');

// Render
const InputText = (props) => {

  const classCtn = props.className ? 'InputText ' + props.className : 'InputText';

  return (
    <div>
      <label htmlFor={props.elId}>{props.label}</label>
      <input
        {...props}
        id={props.elId}
        type="text"
        className={classCtn}
      />
      {(() => {
        if (props.regex.type && (props.regex.type === 'customNumber')) {
          return (
            <Button
              className="resetBtn"
              data-forinput={props.name}
              onClick={props.onResetCustomInput}
            >X</Button>
          );
        }
        return null;
      })()}
      {(() => {
        if (props.message) {
          return (
            <span className={!props.valid ? 'inputField_error' : 'inputField_info'}>
              {'Min. chars ' + props.regex.min + ' - Max. chars ' + props.regex.max}
            </span>
          );
        }
        return null;
      })()}
    </div>
  );

};

InputText.propTypes = {
  className: PropTypes.string,
  elId: PropTypes.string,
  placeholder: PropTypes.string,
  label: PropTypes.string,
  regex: PropTypes.object.required,
  value: PropTypes.string,
  valid: PropTypes.bool,
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
  resetField: PropTypes.func,
  name: React.PropTypes.string,
  message: PropTypes.bool
};

InputText.defaultProps = {
  valid: true,
  message: true
};

export default InputText;
