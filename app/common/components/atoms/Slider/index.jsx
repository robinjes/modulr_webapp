// Librairies
import React, { PropTypes } from 'react';
// Styles
require('./styles');
// Render
const Slider = ({ className, elId, name, globalname, message, value, valid, regex, onChange }) => {

  const classCtn = className ? 'slider ' + className : 'slider';

  return (
    <div>
      <input
        id={elId}
        name={name}
        data-globalname={globalname}
        type="range"
        value={value}
        min={regex.min}
        max={regex.max}
        className={classCtn}
        onChange={onChange}
      />
      <label htmlFor={elId}>{value}</label>

      {(() => {
        if (message) {
          return (
            <span className={!valid ? 'inputField_error' : 'inputField_info'}>
              {'Max. ' + regex.max}
            </span>
          );
        }
        return null;
      })()}

    </div>
  );

};

Slider.propTypes = {
  className: PropTypes.string,
  name: PropTypes.string,
  globalname: PropTypes.string,
  elId: PropTypes.string.isRequired,
  regex: PropTypes.object,
  value: PropTypes.number,
  message: PropTypes.bool,
  valid: PropTypes.bool,
  onChange: PropTypes.func
};

Slider.defaultProps = {
  value: 0,
  min: 0,
  max: 100
};

export default Slider;
