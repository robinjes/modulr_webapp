// Librairies
import React, { PropTypes } from 'react';
// Styles
require('./styles');
// Render
const Modal = ({ className, children, closeFunc, open, dataForinput }) => {

  let classCtn = className ? 'modal ' + className : 'modal';
  classCtn += open ? ' isOpen' : '';

  return (
    <div>
      <div
        data-forinput={dataForinput}
        className={'veil' + (open ? ' isOpen' : '')}
        onClick={closeFunc}
      />
      <div className={classCtn}>{children}</div>
    </div>
  );

};

Modal.propTypes = {
  className: PropTypes.string,
  children: PropTypes.any,
  closeFunc: PropTypes.func,
  open: PropTypes.bool,
  dataForinput: PropTypes.string
};

export default Modal;
