// Librairies
import React, { PropTypes } from 'react';
// Styles
require('./styles');
// Render
const Button = (props) => {

  const { className, children, onClick, position } = props;
  let classCtn = className ? 'button ' + className : 'button';
  classCtn += typeof position !== 'undefined' ? ' pos-' + position : '';

  return (
    <button {...props} onClick={onClick} className={classCtn}>{children}</button>
  );

};

Button.propTypes = {
  className: PropTypes.string,
  children: PropTypes.string,
  onClick: PropTypes.func,
  position: PropTypes.string
};

export default Button;
