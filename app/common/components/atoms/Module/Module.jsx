// Librairies
import React, { PropTypes } from 'react';
// Styles
require('./styles');
// Render
const Module = (props) => {
  const { onClick, rgba, selected, hovered, className, paramfirst, orientation } = props;
  let classCtn = className ? 'module ' + className : 'module',
    /* Built-in style */
    colorStyle = null;

  classCtn += ' ' + orientation;
  classCtn += selected ? ' selected' : '';
  classCtn += hovered ? ' hovered' : '';

  if (rgba && !hovered) {
    colorStyle = {
      backgroundColor: 'rgba(' + String(rgba[0])
                              + ', ' + String(rgba[1]) + ', '
                              + String(rgba[2]) + ', '
                              + String(rgba[3]) + ')'
    };
  }

  return (
    <div
      {...props}
      style={Object.assign({}, props.style, colorStyle)}
      className={classCtn}
      onClick={onClick}
      data-paramfirst={paramfirst}
    />
  );

};

Module.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  paramfirst: React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.number
  ]),
  onClick: PropTypes.func,
  selectedColor: PropTypes.object,
  rgba: PropTypes.array,
  orientation: PropTypes.string.isRequired,
  selected: PropTypes.bool.isRequired,
  hovered: PropTypes.bool
};

export default Module;
