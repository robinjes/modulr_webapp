// Librairies
import React, { PropTypes } from 'react';
// Styles
require('./styles');
// Render
const Message = (props) => {
  const { className, children, type, removable } = props;
  let classCtn = className ? 'message ' + className : 'message';
  classCtn += removable ? ' removable' : '';

  return (
    <div>
      <div className={classCtn}>
        {children}
      </div>
    </div>
  );

};

Message.propTypes = {
  className: PropTypes.string,
  children: PropTypes.string,
  type: PropTypes.string,
  removable: PropTypes.bool
};

Message.defaultProps = {
  type: 'global',
  removable: false
};

export default Message;
