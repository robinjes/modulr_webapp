// Librairies
import React, { PropTypes } from 'react';
// Styles
require('./styles');
// Render
const Canvas = ({ className, id, width, height }) => {

  const classCtn = className ? 'canvas ' + className : 'canvas';

  return (
    <canvas className={classCtn} id={id} width={width} height={height} />
  );

};

Canvas.propTypes = {
  className: PropTypes.string,
  id: PropTypes.string,
  width: PropTypes.number,
  height: PropTypes.number
};

export default Canvas;
