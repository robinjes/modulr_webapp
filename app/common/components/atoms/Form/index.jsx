// Librairies
import React, { PropTypes } from 'react';
// Styles
require('./styles');
// Render
const Form = ({ className, children }) => {

  const classCtn = className ? 'form ' + className : 'form';

  return (
    <form className={classCtn}>
      {children}
    </form>
  );

};

Form.propTypes = {
  className: PropTypes.string,
  children: PropTypes.node
};

export default Form;
