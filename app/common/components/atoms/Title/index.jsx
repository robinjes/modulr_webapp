// Librairies
import React, { PropTypes } from 'react';
// Styles
require('./styles');
// Render
const Title = ({ className, heading, width, align }) => {

  let classCtn = className ? 'title ' + className : 'title';
  classCtn += heading ? ' heading_' + heading : '';
  classCtn += width ? ' width_' + width : '';
  classCtn += align ? ' align_' + align : '';

  return (
    <p className={classCtn} />
  );

};

Title.propTypes = {
  className: PropTypes.string,
  heading: PropTypes.string,
  width: PropTypes.string,
  align: PropTypes.string
};

export default Title;
