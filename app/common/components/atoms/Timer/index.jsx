// Librairies
import React, { PropTypes } from 'react';
// Styles
require('./styles');
// Render
const Timer = (props) => {

  const { className, children } = props,
    classCtn = className ? 'timer ' + className : 'timer';

  return (
    <div className={classCtn}>{children}</div>
  );

};

Timer.propTypes = {
  className: PropTypes.string,
  children: PropTypes.number
};

export default Timer;
