// Librairies
import React, { PropTypes } from 'react';
// Styles
require('./styles');
// Render
const Deck = (props) => {
  const { onClick, rgba, selected, hovered, className, paramfirst } = props;
  let classCtn = className ? 'deck ' + className : 'deck',
    /* Built-in style */
    colorStyle = null;

  classCtn += selected ? ' selected' : '';
  classCtn += hovered ? ' hovered' : '';

  if (rgba && !hovered) {
    colorStyle = {
      backgroundColor: 'rgba(' + String(rgba[0])
                              + ', ' + String(rgba[1]) + ', '
                              + String(rgba[2]) + ', '
                              + String(rgba[3]) + ')'
    };
  }

  return (
    <div
      {...props}
      style={Object.assign({}, props.style, colorStyle)}
      className={classCtn}
      onClick={onClick}
      data-paramfirst={paramfirst}
    />
  );

};

Deck.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  paramfirst: React.PropTypes.oneOfType([
    React.PropTypes.string,
    React.PropTypes.number
  ]),
  onClick: PropTypes.func,
  selectedColor: PropTypes.object,
  rgba: PropTypes.array,
  selected: PropTypes.bool.isRequired,
  hovered: PropTypes.bool
};

export default Deck;
