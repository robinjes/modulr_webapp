// Librairies
import React, { PropTypes } from 'react';
// Components
import Deck from './Deck';
// Render
class DeckCtn extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isHovered: false
    };

    this.handleMouseClick = this.handleMouseClick.bind(this);
    this.handleMouseEnter = this.handleMouseEnter.bind(this);
    this.handleMouseLeave = this.handleMouseLeave.bind(this);

  }

  handleMouseEnter() {
    this.setState({ isHovered: true });
  }

  handleMouseLeave() {
    this.setState({ isHovered: false });
  }

  handleMouseClick() {
    const newColorRef = this.props.selectedColorId;
    this.props.onModuleClick(this.props.id, newColorRef);
  }

  render() {

    return (
      <Deck
        {...this.props}
        onClick={this.handleMouseClick}
        onMouseEnter={this.handleMouseEnter}
        onMouseLeave={this.handleMouseLeave}
        hovered={this.state.isHovered}
      />
    );
  }

}

DeckCtn.propTypes = {
  id: PropTypes.string,
  selectedColorId: PropTypes.string,
  onModuleClick: PropTypes.func
};

export default DeckCtn;
