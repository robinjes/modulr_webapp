// Librairies
import React, { PropTypes } from 'react';
// Components
import Modal from 'components/atoms/Modal';
import Slider from 'components/atoms/Slider';
import Button from 'components/atoms/Button';
// Styles
require('./styles');

// Render
const InputNum = (props) => {

  const classCtn = props.className ? 'inputNum ' + props.className : 'inputNum',
    customValueKeys = Object.keys(props.value),
    elUniqueValue = props.regex.type === 'customNumber'
              ? Object.keys(props.value)[0].customValue
              : props.value;

  return (
    <div>
      <label htmlFor={props.elId}>{props.label}</label>
      <input
        id={props.elId}
        type="number"
        placeholder={props.placeholder}
        value={elUniqueValue}
        name={props.name}
        className={classCtn}
        onBlur={props.onBlur}
        onChange={props.onChange}
      />
      {(() => {
        if (props.regex.type === 'customNumber') {
          return (
            <div>
              <Button className="customBtn" onClick={props.openModal}>Custom</Button>
              <Modal dataForinput={props.name} closeFunc={props.closeModal} open={props.isOpen}>
                {customValueKeys.map(key =>
                  <Slider
                    {...props}
                    key={props.value[key].id}
                    elId={props.name + '_' + props.value[key].customName}
                    value={props.value[key].customValue}
                    name={props.value[key].customName}
                    globalname={props.name}
                    regex={props.value[key].regex}
                    valid
                    onChange={props.onCustomChange}
                    onCustomClick={props.onCustomClick}
                  />
                )}
              </Modal>
            </div>
          );
        }
        return null;
      })()}
      {(() => {
        if (props.message) {
          if (props.regex.type === 'customNumber') {
            return (
              <span className={!props.valid ? 'inputField_error' : 'inputField_info'}>
                {'Only digits - Min. '
                + Math.max(props.value[customValueKeys[0]].regex.min,
                  props.value[customValueKeys[1]].regex.min)
                + ' - Max. '
                + Math.min(props.value[customValueKeys[0]].regex.max,
                  props.value[customValueKeys[1]].regex.max)}
              </span>
            );
          }
          return (
            <span className={!props.valid ? 'inputField_error' : 'inputField_info'}>
              {'Only digits - Min. '
              + props.regex.min
              + ' - Max. '
              + props.regex.max}
            </span>
          );
        }
        return null;
      })()}
    </div>
  );

};

InputNum.propTypes = {
  className: PropTypes.string,
  elId: PropTypes.string,
  placeholder: PropTypes.string,
  label: PropTypes.string,
  regex: PropTypes.object,
  value: PropTypes.oneOfType([
    React.PropTypes.number,
    React.PropTypes.object
  ]),
  valid: PropTypes.bool,
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
  onCustomChange: PropTypes.func,
  isOpen: PropTypes.bool,
  openModal: PropTypes.func,
  closeModal: PropTypes.func,
  name: React.PropTypes.string,
  message: PropTypes.bool
};

InputNum.defaultProps = {
  valid: true,
  message: true,
  value: 0
};

export default InputNum;
