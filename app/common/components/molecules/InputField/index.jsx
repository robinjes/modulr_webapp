// Librairies
import React, { PropTypes } from 'react';
// Components
import InputNum from 'components/atoms/InputNum';
import InputText from 'components/atoms/InputText';
import InputSelect from 'components/atoms/InputSelect';
import InputFile from 'components/atoms/InputFile';
// Styles
require('./styles');
// Render
class InputField extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      inputNumModalOpen: false,
      customValueStr: ''
    };
    this.openInputNumModal = this.openInputNumModal.bind(this);
    this.closeInputNumModal = this.closeInputNumModal.bind(this);
    this.handleOnResetCustomInput = this.handleOnResetCustomInput.bind(this);
  }

  handleOnResetCustomInput(evt) {
    evt.preventDefault();

    this.props.onResetCustomInput(evt);

    this.setState({ customValueStr: '' });
  }

  convertValueToStr(values) {
    let valueStr = '',
      value;

    for (value of Object.keys(values)) {
      valueStr += String(values[value].customName)
                  + ': ' + String(values[value].customValue) + ' / ';
    }
    return valueStr;
  }

  openInputNumModal(evt) {
    evt.preventDefault();
    this.setState({ inputNumModalOpen: true });
  }

  closeInputNumModal() {

    this.setState({
      inputNumModalOpen: false,
      customValueStr: this.convertValueToStr(this.props.value)
    });

    this.props.onCustomBlur(this.props.name);
  }

  render() {
    let inputEl;

    switch (this.props.regex.type) {
      case 'text':
        inputEl = <InputText {...this.props} />;
        break;
      case 'number':
      case 'customNumber':
        inputEl = (
          () => {
            if (this.props.regex.enable === true) {
              return (
                <InputText
                  {...this.props}
                  value={this.state.customValueStr}
                  message={false}
                  disabled
                  onResetCustomInput={this.handleOnResetCustomInput}
                />
              );
            }
            return (
              <InputNum
                {...this.props}
                isOpen={this.state.inputNumModalOpen}
                openModal={this.openInputNumModal}
                closeModal={this.closeInputNumModal}
              />
            );
          })();
        break;
      case 'select':
        inputEl = <InputSelect {...this.props} />;
        break;
      case 'file':
        inputEl = <InputFile {...this.props} />;
        break;
      default:
        inputEl = <InputText {...this.props} />;
    }
    return inputEl;
  }
}

InputField.propTypes = {
  name: PropTypes.string,
  regex: PropTypes.object,
  value: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string,
    PropTypes.object
  ]),
  onCustomBlur: PropTypes.func,
  onResetCustomInput: PropTypes.func
};

export default InputField;
