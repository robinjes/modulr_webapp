/* global FileReader  */
// Librairies
import React, { PropTypes } from 'react';
import FileManager from 'utils/fileManager';
// Components
import InputField from 'components/molecules/InputField';
import Button from 'components/atoms/Button';
// Styles
require('./styles');

// Render
class DropZone extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      bgSrc: '',
      imgAdded: false
    };

    this.fileManager = new FileManager(/image.*/);

    this.handleDrop = this.handleDrop.bind(this);
    this.handleDragOver = this.handleDragOver.bind(this);
    this.handleInput = this.handleInput.bind(this);
    this.emptyZone = this.emptyZone.bind(this);
  }

  componentDidMount() {
    if (!this.fileManager.hasFileReader()) return;
  }

  handleDragOver(e) {
    e.preventDefault();
  }

  handleDrop(e) {
    e.preventDefault();
    this.fileManager.handleDrop(e, (res) => {
      this.setState({ bgSrc: res, imgAdded: true });
      this.dropzoneElement.style.backgroundSize = '100% 100%';
    });
  }

  handleInput(e) {
    e.preventDefault();
    this.fileManager.handleInput(e, (res) => {
      this.setState({ bgSrc: res, imgAdded: true });
      this.dropzoneElement.style.backgroundSize = '100% 100%';
    });
  }

  emptyZone() {
    this.setState({ bgSrc: '', imgAdded: false });
  }

  render() {

    const dropZoneStyle = {
      backgroundImage: 'url(' + this.state.bgSrc + ')'
    };

    return (

      <div
        className="dropZone"
        style={dropZoneStyle}
        onDragOver={this.handleDragOver}
        onDrop={this.handleDrop}
        ref={(dropzone) => { this.dropzoneElement = dropzone; }}
      >
        <InputField
          className="dropZone_file_btn"
          regex={{ type: 'file' }}
          fullZone
          name="files[]"
          onChange={this.handleInput}
        />
        {this.state.imgAdded &&
          <Button onClick={this.emptyZone} position="center">Supprimer image</Button>
        }
      </div>
    );
  }

}

DropZone.propTypes = {
  className: PropTypes.string
};

export default DropZone;
