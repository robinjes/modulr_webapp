// Librairies
import React, { PropTypes } from 'react';
import CameraManager from 'utils/cameraManager';
// Components
import Button from 'components/atoms/Button';
import Timer from 'components/atoms/Timer';
// Styles
require('./styles');

// Render
class PhotoBooth extends React.Component {

  constructor(props) {
    super(props);

    this.state = {
      enable: false,
      countdown: false,
      countdownvalue: 5
    };

    this.cameraManager = new CameraManager();

    this.takeSnapshot = this.takeSnapshot.bind(this);
    this.toggleCamera = this.toggleCamera.bind(this);
  }

  componentDidMount() {
    if (!this.cameraManager.hasGetUserMedia()) return;
    this.cameraManager.initSnapshot(this.canvasElement);
    this.toggleCamera();
  }

  componentWillUnmount() {
    this.cameraManager.disableCamera(() => {
      this.setState({
        userMediaRequested: false
      });
      window.URL.revokeObjectURL(this.state.src);
    });
  }

  startSnapshotTimer(callback) {
    this.setState({ countdown: true });
    const counter = this.state.countdownvalue,
      countDown = (currentTime, callbackFn) => {
        this.setState({ countdownvalue: currentTime });
        if (currentTime !== 0) {
          setTimeout(countDown.bind(null, currentTime - 1), 1000);
        } else {
          this.setState({ countdown: false, countdownvalue: counter });
          if (typeof callback === 'function') {
            callback();
          }
        }
      };

    countDown(counter, callback);
  }

  takeSnapshot() {
    if (this.cameraManager &&
      this.cameraManager.localMediaStream &&
      this.cameraManager.canvas) {
      this.startSnapshotTimer(() => {
        this.cameraManager.ctx.drawImage(this.videoElement, 0, 0);
        this.toggleCamera();
      });
    }
  }

  toggleCamera() {
    if (this.state.enable) {
      this.cameraManager.disableCamera(() => {
        window.URL.revokeObjectURL(this.state.src);
      });
      this.setState({ enable: false });
    } else {
      this.setState({ enable: true });
      this.cameraManager.enableCamera({
        audio: false,
        audioSource: false,
        videoSource: true
      }, this.videoElement);
    }
  }

  render() {

    return (
      <div className="photobooth">
        <video
          ref={(video) => { this.videoElement = video; }}
          {...this.props}
          src={this.state.src}
          className={'videoBooth' + (!this.state.enable ? ' hide' : ' show')}
        />
        <canvas
          ref={(canvas) => { this.canvasElement = canvas; }}
          className={'snapshot' + (this.state.enable ? ' hide' : ' show')}
          width={this.props.width}
          height={this.props.height}
        />
        {this.state.countdown &&
          <Timer>{this.state.countdownvalue}</Timer>
        }
        {!this.state.countdown &&
        <Button
          onClick={this.state.enable ? this.takeSnapshot : this.toggleCamera}
          position="center"
        >
          {this.state.enable ? 'Take a picture!' : 'Back to Camera'}
        </Button>
        }
      </div>
    );
  }

}

PhotoBooth.propTypes = {
  audio: PropTypes.bool,
  muted: PropTypes.bool,
  onUserMedia: PropTypes.func,
  height: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ]),
  width: PropTypes.oneOfType([
    PropTypes.number,
    PropTypes.string
  ]),
  screenshotFormat: PropTypes.oneOf([
    'image/webp',
    'image/png',
    'image/jpeg'
  ]),
  className: PropTypes.string
};

PhotoBooth.defaultProps = {
  audio: true,
  height: 480,
  width: 640,
  screenshotFormat: 'image/webp',
  onUserMedia: () => {}
};

export default PhotoBooth;
