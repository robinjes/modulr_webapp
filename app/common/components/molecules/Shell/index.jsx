// Librairies
import React, { PropTypes } from 'react';
// Components
import Header from 'components/containers/Header';
// Styles
require('./styles');

class Shell extends React.Component {
  constructor(props) {
    super(props);
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log('Shell updates: ', nextProps !== this.props);
    return nextProps !== this.props;
  }

  render() {
    const className = this.props.className ?
                    this.props.className + ' shell-container'
                    : 'shell-container';
    return (
      <div className={className}>
        <Header />
        {this.props.children}
        <p>Olfraime's Modul'R 2017</p>
      </div>
    );

  }
}

Shell.propTypes = {
  className: PropTypes.string,
  children: PropTypes.any
};

export default Shell;
