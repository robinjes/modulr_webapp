// Librairies
import React, { PropTypes } from 'react';
// Components
import MenuItemList from './MenuItemList';
// Styles
require('./styles');

class MenuItemCtrl extends React.Component {
  constructor(props) {
    super(props);

    const itemListClassName = this.props.itemListClassName ?
                              this.props.itemListClassName + ' hide' :
                              'hide';

    this.state = {
      itemListClassName,
      openedList: false
    };

    this.handleOpenList = this.handleOpenList.bind(this);
    this.handleCloseList = this.handleCloseList.bind(this);
    this.handleToggleList = this.handleToggleList.bind(this);

    this.handleListItemClick = this.handleListItemClick.bind(this);
  }

  handleToggleList() {
    if (this.state.openedList) {
      this.handleCloseList();
    } else {
      this.handleOpenList();
    }
  }

  handleOpenList() {
    const itemListClassName = this.props.itemListClassName ?
                              this.props.itemListClassName + ' show' :
                              'show';
    this.setState({ itemListClassName, openedList: true });
  }

  handleCloseList() {
    const itemListClassName = this.props.itemListClassName ?
                              this.props.itemListClassName + ' hide' :
                              'hide';
    this.setState({ itemListClassName, openedList: false });
  }

  handleListItemClick(evt) {
    console.log('handleListItemClick: ', evt.target.dataset.funcname);
    console.log(this.props);
  }

  render() {
    const { className, type, label, action } = this.props,
      classCtn = className ? 'menuItem ' + className : 'menuItem';
    let extraComponent = null;

    switch (type) {
      case 'submenu':
        extraComponent =
          (<MenuItemList
            className={this.state.itemListClassName}
            onSubClick={this.handleListItemClick}
            items={this.props.items}
          />);
        break;
      case 'multichoices':
        extraComponent = () => (
          <MenuItemList
            className={this.props.itemListClassName}
            onSubClick={this.handleListItemClick}
            items={this.props.items}
          />
        );
        break;
      default:
        extraComponent = null;
    }

    return (
      <div className={classCtn}>
        <div onClick={extraComponent !== null ? this.handleToggleList : action}>
          {label}
        </div>
        {extraComponent}
      </div>
    );

  }
}

MenuItemCtrl.propTypes = {
  className: PropTypes.string,
  itemListClassName: PropTypes.string,
  type: PropTypes.string,
  label: PropTypes.string,
  items: PropTypes.array,
  action: PropTypes.func
};

export default MenuItemCtrl;
