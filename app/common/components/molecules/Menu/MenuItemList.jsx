// Librairies
import React, { PropTypes } from 'react';
// Styles
require('./styles');
// Render
const MenuItemList = (props) => {

  const { className, items, onSubClick } = props,
    classCtn = className ? 'menuItemList ' + className : 'menuItemList';

  return (
    <div className={classCtn}>
      {items && items.map(item =>
        <div key={item.id} onClick={item.action}>{item.label}</div>
      )}
    </div>
  );

};

MenuItemList.propTypes = {
  className: PropTypes.string,
  items: PropTypes.array,
  onSubClick: PropTypes.func
};

export default MenuItemList;
