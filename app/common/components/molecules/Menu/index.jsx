// Librairies
import React, { PropTypes } from 'react';
// Components
import MenuItem from './MenuItemCtrl';
// Styles
require('./styles');
// Render
const Menu = (props) => (

  <div className={props.className}>
    {props.items && props.items.map(item =>
      <MenuItem {...item} key={item.id} />
    )}
  </div>

);

Menu.propTypes = {
  className: PropTypes.string,
  items: PropTypes.array
};

export default Menu;
