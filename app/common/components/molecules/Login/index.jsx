// Librairies
import React, { PropTypes, Component } from 'react';
import { connect } from 'react-redux';
// Components
import Modal from 'components/atoms/Modal';

// Render
class LoginModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
    this.handleOnChange = this.handleOnChange.bind(this);
    this.handleOnSubmit = this.handleOnSubmit.bind(this);
  }

  handleOnChange(e) {
    const name = e.target.name;
    const value = e.target.value;
    this.setState({ [name]: value });
  }

  handleOnSubmit(e) {
    e.preventDefault();
    this.props.onSubmit(this.state);
  }

  render() {
    return (
      <Modal
        class="loginModal"
        open={this.props.open}
        closeFunc={this.props.closeFunc}
      >
        <form onSubmit={this.handleOnSubmit}>
          <label htmlFor="username">Username or email:</label>
          <input onChange={this.handleOnChange} name="username" type="text" />
          <label htmlFor="password">Password:</label>
          <input onChange={this.handleOnChange} name="password" type="text" />
          <button>Login</button>
        </form>
      </Modal>
    );
  }
}

LoginModal.propTypes = {
  open: PropTypes.bool,
  closeFunc: PropTypes.func,
  onSubmit: PropTypes.func,
  children: PropTypes.any
};

export default LoginModal;
