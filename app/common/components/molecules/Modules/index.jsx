// Librairies
import React, { PropTypes } from 'react';
// Components
import Loader from 'components/atoms/Loader';
import Modules from './Modules';

// Render
class ModulesCtn extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      /* Get default color if none selected */
      selectedColor: typeof props.selectedColor === 'undefined'
                     ? props.defaultColor
                     : props.selectedColor
    };

  }
  componentDidMount() {
    const { createModules, algo } = this.props;
    createModules(algo);
  }

  render() {
    const { isFetching, modules, selectedColor, defaultColor } = this.props,
      selectedColorBuff = typeof selectedColor === 'undefined'
                           ? defaultColor
                           : selectedColor;

    if (isFetching) {
      return <Loader>Meanwhile...</Loader>;
    }
    if (!isFetching && modules.valid) {
      return (
        <Modules
          {...this.props}
          selectedColor={selectedColorBuff}
          modules={this.props.modules.items}
        />
      );
    }

    return null;

  }

}

ModulesCtn.propTypes = {
  isFetching: PropTypes.bool,
  className: PropTypes.string,
  algo: PropTypes.object,
  modules: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array
  ]).isRequired,
  defaultColor: PropTypes.object,
  selectedColor: PropTypes.object,
  createModules: PropTypes.func
};

export default ModulesCtn;
