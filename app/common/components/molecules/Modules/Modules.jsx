// Librairies
import React, { PropTypes } from 'react';
// Components
import Module from 'components/atoms/Module';
import Deck from 'components/atoms/Deck';
// Styles
require('./styles');
// Render
const Modules = (props) => {

  const { ratio, modules, selectedColor, onModuleClick,
          moduleRefWidth, moduleRefHeight, deckRefWidth,
          className } = props,


    ratioFlt = parseFloat(ratio),
    moduleRefWidthFlt = ratioFlt * parseFloat(moduleRefWidth),
    moduleRefHeightFlt = ratioFlt * parseFloat(moduleRefHeight),
    deckRefWidthFlt = ratioFlt * parseFloat(deckRefWidth),
    // decks & modules
    tlDeck = modules.tl[0],
    trDeck = modules.tr[0],
    brDeck = modules.br[0],
    blDeck = modules.bl[0],
    tModules = modules.t,
    tModulesLen = tModules.length,
    tSideLen = tModulesLen * moduleRefWidthFlt + deckRefWidthFlt * 2,
    sideMargin = deckRefWidthFlt - moduleRefHeightFlt,
    rModules = modules.r,
    bModules = modules.b,
    lModules = modules.l,
    lModulesLen = lModules.length,
    lSideLen = lModulesLen * moduleRefWidthFlt + deckRefWidthFlt * 2,
    // define styles
      // MAIN BLOCS
      // Left (vSide), Center & Right (vSide) blocks
    modulesCtnStyle = {
      width: String(tSideLen) + '%',
      height: String(lSideLen) + '%',
      top: String((100 - lSideLen) / 2) + '%',
      left: String((100 - tSideLen) / 2) + '%'
    },
    vSideStyle = {
      width: String(100 * deckRefWidthFlt / tSideLen) + '%',
      height: '100%'
    },
    centerSideStyle = {
      width: String(100 * (tModulesLen * moduleRefWidthFlt) / tSideLen) + '%',
      height: '100%'
    },
      // INNER BLOCS
      // Inner-Center side
    centerEmptySideStyle = {
      height: String(100 * lModulesLen * moduleRefWidthFlt / lSideLen) + '%'
    },
      // Inner-Center Top & Bottom sides
    hSideStyle = {
      height: String(100 * deckRefWidthFlt / lSideLen) + '%'
    },
      // Decks & Modules
    deckSizeStyle = {
      height: hSideStyle.height
    },
        // left & right modules
    hModuleSizeStyle = {
      width: String(100 * moduleRefHeightFlt / deckRefWidthFlt) + '%',
      height: String(100 * moduleRefWidthFlt / lSideLen) + '%'
    },
    rightSideModuleStyle = {
      left: String(100 * sideMargin / deckRefWidthFlt) + '%'
    },
        // top & bottom modules
    vModuleSizeStyle = {
      width: String(100 / tModulesLen) + '%',
      height: hModuleSizeStyle.width
    },
    bottomSideModuleStyle = {
      top: rightSideModuleStyle.left
    },

    classCtn = className ? 'modules show ' + className : 'modules show';

  return (
    <div className={classCtn} style={modulesCtnStyle}>
      <div className="leftSide" style={vSideStyle}>
        <Deck
          style={deckSizeStyle}
          selectedColorId={selectedColor.id}
          rgba={props.colors[tlDeck.colorRef].rgba}
          onModuleClick={onModuleClick}
          paramfirst={tlDeck.id}
          {...tlDeck}
        />
        {lModules.map(module =>
          <Module
            key={module.id}
            {...module}
            style={hModuleSizeStyle}
            selectedColorId={selectedColor.id}
            rgba={props.colors[module.colorRef].rgba}
            onModuleClick={onModuleClick}
            orientation="horizontal"
            paramfirst={module.id}
          />
        )}
        <Deck
          style={deckSizeStyle}
          selectedColorId={selectedColor.id}
          rgba={props.colors[blDeck.colorRef].rgba}
          onModuleClick={onModuleClick}
          paramfirst={blDeck.id}
          {...blDeck}
        />
      </div>

      <div className="centerSide" style={centerSideStyle}>
        <div className="centerTopSide" style={hSideStyle}>
          {tModules.map(module =>
            <Module
              key={module.id}
              {...module}
              style={vModuleSizeStyle}
              selectedColorId={selectedColor.id}
              rgba={props.colors[module.colorRef].rgba}
              onModuleClick={onModuleClick}
              orientation="vertical"
              paramfirst={module.id}
            />
          )}
        </div>
        <div className="centerEmptySide" style={centerEmptySideStyle} />
        <div className="centerBottomSide" style={hSideStyle}>
          {bModules.map(module =>
            <Module
              key={module.id}
              {...module}
              style={Object.assign({}, vModuleSizeStyle, bottomSideModuleStyle)}
              selectedColorId={selectedColor.id}
              rgba={props.colors[module.colorRef].rgba}
              onModuleClick={onModuleClick}
              orientation="vertical"
              paramfirst={module.id}
            />
          )}
        </div>
      </div>
      <div className="rightSide" style={vSideStyle}>
        <Deck
          style={deckSizeStyle}
          selectedColorId={selectedColor.id}
          rgba={props.colors[trDeck.colorRef].rgba}
          onModuleClick={onModuleClick}
          paramfirst={trDeck.id}
          {...trDeck}
        />
        {rModules.map(module =>
          <Module
            key={module.id}
            {...module}
            style={Object.assign({}, hModuleSizeStyle, rightSideModuleStyle)}
            selectedColorId={selectedColor.id}
            rgba={props.colors[module.colorRef].rgba}
            onModuleClick={onModuleClick}
            orientation="horizontal"
            paramfirst={module.id}
          />
        )}
        <Deck
          style={deckSizeStyle}
          selectedColorId={selectedColor.id}
          rgba={props.colors[brDeck.colorRef].rgba}
          onModuleClick={onModuleClick}
          paramfirst={brDeck.id}
          {...brDeck}
        />
      </div>
    </div>
  );

};

Modules.propTypes = {
  className: PropTypes.string,
  ratio: PropTypes.number,
  selectedColor: PropTypes.object,
  colors: PropTypes.object,
  onModuleClick: PropTypes.func,
  modules: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array
  ]).isRequired,
  moduleRefHeight: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]).isRequired,
  moduleRefWidth: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]).isRequired,
  deckRefWidth: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.number
  ]).isRequired
};

export default Modules;
