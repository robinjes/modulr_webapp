// Librairies
import React, { PropTypes } from 'react';
// Components
import ColorSquareCtn from 'components/atoms/ColorSquare/index.jsx';
// Styles
require('./styles');
// Render
const ColorChart = (props) => {

  const { colors, className } = props,
    classCtn = className ? 'colorChart full-height' + className : 'colorChart full-height';

  return (
    <div className={classCtn}>
      <div className="colors-row">
        {Object.keys(colors).map(colorKey =>
          <ColorSquareCtn
            {...props}
            key={colors[colorKey].id}
            color={colors[colorKey]}
          />
        )}
      </div>
    </div>
  );

};

ColorChart.propTypes = {
  className: PropTypes.string,
  colors: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array
  ]).isRequired
};

export default ColorChart;
