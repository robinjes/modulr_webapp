// Librairies
import React, { PropTypes } from 'react';
// Components
import ColorChart from './ColorChart';
// Render
class ColorChartCtn extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {

    return <ColorChart {...this.props} />;

  }

}

ColorChartCtn.propTypes = {
  className: PropTypes.string,
  colors: PropTypes.oneOfType([
    React.PropTypes.object,
    React.PropTypes.array
  ]).isRequired
};

export default ColorChartCtn;
