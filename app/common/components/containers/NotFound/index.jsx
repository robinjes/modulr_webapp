import React from 'react';

const NotFound = props => {

  const className = props.className ? props.className + ' notFound' : 'notFound';

  return (
    <div className={className}>
      <h1>Not found</h1>
    </div>
  );
};

NotFound.propTypes = {
  className: React.PropTypes.string
};

export default NotFound;
