// Librairies
import { connect } from 'react-redux';
// Utils
import { getToken } from 'utils/userManager';
// Actions
import {
  saveFrame,
  login
} from 'actions/asyncs';
import {
  updateFrameName,
    updateFrameStatus,
    openModal,
    closeModal
} from 'actions/publics';
// Components
import Header from './Header';

// Maps
const mapStateToProps = (state) => ({
    modalsStatus: state.application.modals,
    frameName: state.frames.wip.name,
    frameStatus: state.frames.wip.isPublic,
    logged: state.application.logged,
  }),
  mapDispatchToProps = (dispatch) => ({
    onFrameNameUpdate: (name) => {
      dispatch(updateFrameName(name));
    },
    onFrameStatusUpdate: (status) => {
      dispatch(updateFrameStatus(status));
    },
    onSave: () => {
      const token = getToken();
      dispatch(saveFrame(token));
    },
    onLogin: (data) => {
      dispatch(login(data.username, data.password));
    },
    openModal: (type) => {
      dispatch(openModal(type));
    },
    closeModal: (type) => {
      dispatch(closeModal(type));
    }
  }),
  // Connect
  HeaderConnector = connect(
    mapStateToProps,
    mapDispatchToProps
  )(Header);

export default HeaderConnector;
