// Librairies
import React, { PropTypes } from 'react';
// Components
import Menu from 'components/molecules/Menu';
import Modal from 'components/atoms/Modal';
import LoginModal from 'components/molecules/Login';
import InputText from 'components/atoms/InputText';
import Button from 'components/atoms/Button';
import Checkbox from 'components/atoms/Checkbox';
// Styles
require('./styles');

class Header extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};

    this.handleOpenSavingModal = this.handleOpenSavingModal.bind(this);
    this.handleCloseSavingModal = this.handleCloseSavingModal.bind(this);
    this.handleOpenLoginModal = this.handleOpenLoginModal.bind(this);
    this.handleCloseLoginModal = this.handleCloseLoginModal.bind(this);

    this.handleSaveAction = this.handleSaveAction.bind(this);
    this.handleLoginAction = this.handleLoginAction.bind(this);
    this.handleFrameNameUpdate = this.handleFrameNameUpdate.bind(this);
    this.handleFrameStatusUpdate = this.handleFrameStatusUpdate.bind(this);

  }

  componentWillReceiveProps(nextProps) {
    // if one or both change
    if (!((nextProps.frameName === this.state.frameName) &&
        (nextProps.frameStatus === this.state.frameStatus))) {
      this.setState({ frameName: nextProps.frameName, frameStatus: nextProps.frameStatus });
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    return nextProps !== this.props;
  }

  handleSaveAction() {
    const saveActionPromise = Promise.resolve();
    saveActionPromise.then(() => {
      this.props.onFrameNameUpdate(this.state.frameName);
      this.props.onFrameStatusUpdate(this.state.frameStatus);
    })
    .then(() => {
      this.props.onSave();
      this.handleCloseSavingModal();
    });
  }

  handleLoginAction(data) {
    this.props.onLogin(data);
  }

  handleOpenSavingModal() {
    this.props.openModal('saveAction');
  }

  handleCloseSavingModal() {
    this.props.closeModal('saveAction');
  }

  handleOpenLoginModal() {
    this.props.openModal('loginAction');
  }

  handleCloseLoginModal() {
    this.props.closeModal('loginAction');
  }

  handleFrameNameUpdate(e) {
    this.setState({ frameName: e.target.value });
  }

  handleFrameStatusUpdate(e) {
    const status = e.target.checked ? 1 : 0;
    this.setState({ frameStatus: status });
  }

  render() {
    const className = this.props.className ?
                    this.props.className + ' header-container'
                    : 'header-container',
      menuItems = [
        {
          id: '0',
          type: 'submenu',
          label: 'File',
          items: [
            { id: '00', label: 'Open' },
            { id: '01', label: 'Save', action: this.handleOpenSavingModal }
          ]
        },
        {
          id: '1',
          label: 'PDF'
        },
        {
          id: '2',
          className: this.props.logged ? 'btn-wrn' : 'btn-vld',
          label: this.props.logged ? 'Logged as' : 'Login',
          action: this.handleOpenLoginModal
        }
      ];

    return (
      <div className={className}>
        <Menu items={menuItems} />
        <Modal
          class="savingModal"
          open={this.props.modalsStatus.saveAction.enable}
          closeFunc={this.handleCloseSavingModal}
        >
          <InputText
            regex={{}}
            placeholder="Title"
            value={this.state.frameName}
            onChange={this.handleFrameNameUpdate}
          />
          <Checkbox
            label="I choose to make my frame accessible for anyone."
            checked={this.state.frameStatus}
            onChange={this.handleFrameStatusUpdate}
          />
          <Button onClick={this.handleCloseSavingModal}>Cancel</Button>
          <Button onClick={this.handleSaveAction}>Save</Button>
        </Modal>
        <LoginModal
          class="loginModal"
          onSubmit={this.handleLoginAction}
          open={this.props.modalsStatus.loginAction.enable}
          closeFunc={this.handleCloseLoginModal}
        />
      </div>
    );
  }
}

Header.propTypes = {
  className: PropTypes.string,
  frameName: PropTypes.string,
  frameStatus: PropTypes.number,
  onSave: PropTypes.func,
  onFrameStatusUpdate: PropTypes.func,
  onFrameNameUpdate: PropTypes.func,
  openModal: PropTypes.func,
  closeModal: PropTypes.func,
  onLogin: PropTypes.func,
  modalsStatus: PropTypes.objectOf(PropTypes.object),
  logged: PropTypes.bool
};

Header.defaultProps = {
  frameName: 'Untitled',
  frameStatus: 0
};

export default Header;
