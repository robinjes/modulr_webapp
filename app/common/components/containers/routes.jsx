import React from 'react';
import { Router, browserHistory } from 'react-router/es6';
import App from 'components/environments/App';
import configureStore from 'common/store/configureStore';
import * as api from 'utils/api';

function errorLoading(err) {
  console.error('Dynamic page loading failed', err);
}

function loadRoute(cb) {
  return (module) => cb(null, module.default);
}

const routes = {
    component: App,
    childRoutes: [
      {
        path: '/',
        getComponent(location, cb) {
          System.import('components/containers/Home')
            .then(loadRoute(cb))
            .catch(errorLoading);
        }
      },
      {
        path: 'workspace',
        getComponent(location, cb) {
          System.import('components/containers/Workspace')
            .then(loadRoute(cb))
            .catch(errorLoading);
        }
      },
      {
        path: '*',
        getComponent(location, cb) {
          System.import('components/containers/NotFound')
            .then(loadRoute(cb))
            .catch(errorLoading);
        }
      }
    ]
  },
  store = configureStore();

api.setStore(store);

export { store };
export default () => <Router history={browserHistory} routes={routes} />;
