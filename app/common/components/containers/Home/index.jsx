// Librairies
import { connect } from 'react-redux';
// Actions
import { validateValue, saveCustom, resetCustom,
  updateCustomRegex, resetWip, resetModules } from 'actions/publics';
import { fetchAllFrames } from 'actions/asyncs';
// Components
import HomeCtn from './HomeCtn';

  // Maps
const mapStateToProps = (state) => ({
    properties: state.properties,
    references: state.references
  }),
  mapDispatchToProps = (dispatch) => ({
    saveCustomField: (name, value) => {
      dispatch(saveCustom(name, value));
    },
    resetCustomField: (name) => {
      dispatch(resetCustom(name));
    },
    onFieldBlur: (valueName, value, selectedIndex) => {
      dispatch(validateValue(valueName, value, selectedIndex));
    },
    updateCustomRegex: (name, customName, regexName, regexValue) => {
      dispatch(updateCustomRegex(name, customName, regexName, regexValue));
    },
    resetWorkspace: () => {
      dispatch(resetWip());
      dispatch(resetModules());
    },
    fetchAllFrames: () => {
      // dispatch(fetchAllFrames());
    }
  }),
  // Connect
  HomeConnector = connect(
    mapStateToProps,
    mapDispatchToProps
  )(HomeCtn);

export default HomeConnector;
