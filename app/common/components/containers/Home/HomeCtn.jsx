// Librairies
import React, { PropTypes } from 'react';
import { Link } from 'react-router';
// Components
import InitFormCtn from 'components/organisms/InitForm';
import Button from 'components/atoms/Button';

// Styles
require('./styles');

class Home extends React.Component {
  constructor(props) {
    super(props);
  }

  componentDidMount() {
    this.props.fetchAllFrames();
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log('HomeCtn updates: ', nextProps !== this.props);
    return nextProps !== this.props;
  }

  render() {
    const className = this.props.className ?
                    this.props.className + ' home-container'
                    : 'home-container';
    return (
      <div className={className}>
        <h1>Home</h1>
        <InitFormCtn {...this.props} />
        <Link to="/workspace">
          <Button>Create</Button>
        </Link>
      </div>
    );

  }
}

Home.propTypes = {
  className: PropTypes.string,
  resetWorkspace: PropTypes.func,
  fetchAllFrames: PropTypes.func,
  wip: PropTypes.object
};

export default Home;
