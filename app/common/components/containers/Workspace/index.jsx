// Librairies
import { connect } from 'react-redux';
// Actions
import { changeModuleColor, selectColor } from 'actions/publics';
import { createFrame, createModules } from 'actions/asyncs';
// Components
import WorkspaceCtn from './WorkspaceCtn';

// Maps
const mapStateToProps = (state) => ({
    frame: state.frames.wip,
    docWidth: state.properties.artworkWidthProp.value,
    docHeight: state.properties.artworkHeightProp.value,
    vMat: state.properties.matSizeProp.value.horiSizeProp.customValue,
    hMat: state.properties.matSizeProp.value.vertiSizeProp.customValue,
    moduleRef: state.references.moduleRef.regex,
    deckRef: state.references.deckRef.regex,
    algo: state.frames.wip.algo,
    modules: state.modules,
    defaultColor: state.colors.items[Object.keys(state.colors.items)[0]],
    selectedId: state.colors.selectedId,
    selectedColor: state.colors.items[state.colors.selectedId],
    colors: state.colors.items,
    isFetching: state.modules.isFetching || state.frames.isFetching
  }),
  mapDispatchToProps = (dispatch) => ({
    createFrame: (docWidth, docHeight, vMat, hMat, moduleRef, deckRef, defaultColor) => {
      dispatch(createFrame(docWidth, docHeight, vMat, hMat, moduleRef, deckRef, defaultColor));
    },
    createModules: (algo) => {
      dispatch(createModules(algo));
    },
    onModuleClick: (id, colorRef) => {
      dispatch(changeModuleColor(id, colorRef));
    },
    onColorClick: (id) => {
      dispatch(selectColor(id));
    },
  }),
  // Connect
  WorkspaceConnector = connect(
    mapStateToProps,
    mapDispatchToProps
)(WorkspaceCtn);

// Export
export default WorkspaceConnector;
