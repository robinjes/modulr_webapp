// Librairies
import React, { PropTypes } from 'react';
import { Link } from 'react-router';
// Components
import FrameCtn from 'components/organisms/Frame';
import ModulesCtn from 'components/molecules/Modules';
import ColorChartCtn from 'components/molecules/ColorChart';
// Styles
require('./styles');

class Workspace extends React.Component {
  constructor(props) {
    super(props);

    this.setRatio = this.setRatio.bind(this);

    this.cancelDrop = this.cancelDrop.bind(this);
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log('WorkspaceCtn updates: ', nextProps !== this.props);
    return nextProps !== this.props;
  }

  // define ratio
  setRatio(width, height) {
    const frameWidth = parseFloat(width) || 0,
      frameHeight = parseFloat(height) || 0;
    let ratio = 1;

    if ((frameWidth > 0) && (frameHeight > 0)) {

      // if h > v
      if (frameWidth > frameHeight) {
        ratio = 100 / frameWidth;
      } else if (frameWidth < frameHeight) {
        // if h < v
        ratio = 100 / frameHeight;
      } else {
        // if h = v
        ratio = 100 / frameWidth;
      }

    }

    console.log('Workspace: ', ratio);

    return ratio;

  }

  cancelDrop(e) {
    e.preventDefault();
    return false;
  }

  render() {
    const className = this.props.className ?
                    this.props.className + ' workspace'
                    : 'workspace main-container',

      ratio = this.setRatio(this.props.frame.width, this.props.frame.height);

    return (
      <div onDrop={this.cancelDrop} onDragOver={this.cancelDrop}>
        <h1>Workspace</h1>
        <Link to="/">Home</Link>
        <div className={className}>
          <FrameCtn {...this.props} ratio={ratio}>
            <ModulesCtn
              algo={this.props.algo}
              ratio={ratio}
              defaultColor={this.props.defaultColor}
              selectedColor={this.props.selectedColor}
              colors={this.props.colors}
              modules={this.props.modules}
              moduleRefWidth={this.props.moduleRef.width}
              moduleRefHeight={this.props.moduleRef.height}
              deckRefWidth={this.props.deckRef.width}
              createModules={this.props.createModules}
              onModuleClick={this.props.onModuleClick}
            />
          </FrameCtn>
          <ColorChartCtn colors={this.props.colors} onColorClick={this.props.onColorClick} />
        </div>
      </div>
    );

  }
}

Workspace.propTypes = {
  className: PropTypes.string,
  frame: PropTypes.object,
  algo: PropTypes.object,
  defaultColor: PropTypes.object,
  selectedColor: PropTypes.object,
  modules: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array
  ]).isRequired,
  colors: PropTypes.oneOfType([
    PropTypes.object,
    PropTypes.array
  ]).isRequired,
  moduleRef: PropTypes.object,
  deckRef: PropTypes.object,
  createModules: PropTypes.func,
  onModuleClick: PropTypes.func,
  onColorClick: PropTypes.func
};

export default Workspace;
