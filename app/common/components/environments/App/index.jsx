/* Librairies */
import React from 'react';
import { Provider } from 'react-redux';
/* Components */
// import Root from 'components/containers/routes';
import Shell from 'components/molecules/Shell';
/* Store */
// import frameStore from 'common/store';
import Root, { store } from 'components/containers/routes';
// Styles
require('stylesheets/commons');

const App = () => (
  <Provider store={store}>
    <Shell>
      <Root />
    </Shell>
  </Provider>
);

export default App;
