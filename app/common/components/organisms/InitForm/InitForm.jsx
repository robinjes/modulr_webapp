// Librairies
import React, { PropTypes } from 'react';
// Components
import Form from 'components/atoms/Form';
import Title from 'components/atoms/Title';
import InputField from 'components/molecules/InputField';
import Checkbox from 'components/atoms/Checkbox';
// Styles
require('./styles');

// Render
class InitForm extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      artwork_width: props.properties.artworkWidthProp.value,
      artwork_height: props.properties.artworkHeightProp.value,
      artwork_type: props.properties.artworkTypeProp,
      mat_size: props.properties.matSizeProp.value,
      frame_maxWidth: props.references.frameRef.regex.width.max,
      frame_maxHeight: props.references.frameRef.regex.height.max,
      module_height: props.references.moduleRef.regex.height,
      module_offset: props.references.moduleRef.regex.offset,
      activeOption_mat: false
    };

    this.handleInputBlur = this.handleInputBlur.bind(this);
    this.handleCustomInputBlur = this.handleCustomInputBlur.bind(this);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleCustomInputChange = this.handleCustomInputChange.bind(this);
    this.handleCustomInputReset = this.handleCustomInputReset.bind(this);
    this.handleCheckboxChange = this.handleCheckboxChange.bind(this);
  }

  handleInputChange(evt) {
    const element = evt.target,
      inputName = element.name,
      inputValue = parseFloat(element.value);

    if (inputName === 'mat_size') {
      const mutatedValue = this.state.mat_size;

      for (let i = 0; i < mutatedValue.length; i++) {
        mutatedValue[i].customValue = inputValue;
      }
      this.setState({ [inputName]: mutatedValue });
    } else {
      this.setState({ [inputName]: inputValue });
    }
  }

  handleInputBlur(evt) {
    const element = evt.target,
      inputName = element.name,
      regex = { selectedIndex: element.selectedIndex };

    this.props.onFieldBlur(inputName, this.state[inputName], regex);
    this.props.resetWorkspace();
    // close mat option on other field modification
    if ((inputName !== 'mat_size') && (this.state.activeOption_mat === true)) {
      this.setState({ activeOption_mat: false });
      this.props.resetCustomField('mat_size');
    }
  }

  handleCustomInputChange(evt) {
    const element = evt.target,
      inputGlobalName = element.dataset.globalname,
      inputName = element.name,
      inputValue = parseFloat(element.value),
      currentGlobalValue = this.state[inputGlobalName],
      mutatedValue = currentGlobalValue;

    for (let i = 0; i < mutatedValue.length; i++) {
      mutatedValue[i].customValue = mutatedValue[i].customName === inputName
      ? inputValue
      : mutatedValue[i].customValue;
    }

    this.setState({ [inputGlobalName]: mutatedValue });
  }

  handleCustomInputBlur(inputName) {
    this.props.saveCustomField(inputName, this.state.mat_size);
  }

  handleCustomInputReset(evt) {
    const element = evt.target,
      inputName = element.dataset.forinput;

    this.props.resetCustomField(inputName);
  }

  handleCheckboxChange(evt) {
    const element = evt.target,
      isChecked = element.checked,
      elementName = element.name;

    switch (elementName) {
      case 'addMat':
        /* Calculate max mat size - begin */
        if (isChecked === true) {

          if ((this.props.properties.artworkWidthProp.valid)
          && (this.props.properties.artworkHeightProp.valid)
          && (this.props.properties.artworkTypeProp.valid)) {

            const frameMaxHalfWidth = this.state.frame_maxWidth ? this.state.frame_maxWidth / 2 : 0,
              frameMaxHalfHeight = this.state.frame_maxHeight ? this.state.frame_maxHeight / 2 : 0,
              artworkHalfWidth = this.state.artwork_width ? this.state.artwork_width / 2 : 0,
              artworkHalfHeight = this.state.artwork_height ? this.state.artwork_height / 2 : 0,
              modulePracticalHeight = this.state.module_height - this.state.module_offset,
              matMaxHSide = frameMaxHalfWidth - modulePracticalHeight - artworkHalfWidth,
              matMaxVSide = frameMaxHalfHeight - modulePracticalHeight - artworkHalfHeight;

            if ((matMaxHSide >= this.state.mat_size[0].regex.min)
              && (matMaxVSide >= this.state.mat_size[1].regex.min)) {
              this.props.updateCustomRegex('mat_size', 'horizontal', 'max', matMaxHSide);
              this.props.updateCustomRegex('mat_size', 'vertical', 'max', matMaxVSide);
              this.setState({ activeOption_mat: true });
            } else {
              this.setState({ activeOption_mat: false });
              alert('Not allow to add mat.');
            }

          } else {
            this.setState({ activeOption_mat: false });
            alert('Vous devez avoir rempli les champs précédents pour ajouter un passe-partout.');
          }

        } else {
          this.setState({ activeOption_mat: false });
        }
        /* Calculate max mat size - end */
        break;

      default:
        // do nothing
    }

  }

  render() {

    const classCtn = this.props.className ? 'initForm ' + this.props.className : 'initForm';

    return (
      <section className={classCtn}>
        <Form>
          {/* Document */}
          <fieldset>
            <Title heading="3" align="center">Oeuvre/Document</Title>
            <InputField
              elId={'frameWidthInit' + this.props.properties.artworkWidthProp.name}
              {...this.props.properties.artworkWidthProp}
              value={this.state.artwork_width}
              onBlur={this.handleInputBlur}
              onChange={this.handleInputChange}
            />
            <InputField
              elId={'frameWidthInit' + this.props.properties.artworkHeightProp.name}
              {...this.props.properties.artworkHeightProp}
              value={this.state.artwork_height}
              onBlur={this.handleInputBlur}
              onChange={this.handleInputChange}
            />
            <InputField
              elId={'documentTypeInit' + this.props.properties.artworkTypeProp.name}
              {...this.props.properties.artworkTypeProp}
              onBlur={this.handleInputBlur}
              onBlur={this.handleInputBlur}
              onChange={this.handleInputChange}
            />
            <Checkbox
              elId="tickAddMat"
              name="addMat"
              label="Ajouter un passe-partout."
              checked={this.state.activeOption_mat ? 'checked' : false}
              onChange={this.handleCheckboxChange}
            />
          </fieldset>
          {/* Mat */}
          <fieldset className={this.state.activeOption_mat ? 'show ' : 'hide '}>
            <Title heading="3" align="center">Passe-partout</Title>
            <InputField
              elId={'matSizeInit' + this.props.properties.matSizeProp.name}
              {...this.props.properties.matSizeProp}
              onBlur={this.handleInputBlur}
              onCustomBlur={this.handleCustomInputBlur}
              onCustomChange={this.handleCustomInputChange}
              onResetCustomInput={this.handleCustomInputReset}
              onChange={this.handleInputChange}
            />
          </fieldset>
        </Form>
      </section>
    );
  }

}

InitForm.propTypes = {
  className: PropTypes.string,
  properties: PropTypes.object,
  references: PropTypes.object,
  onFieldBlur: PropTypes.func,
  resetWorkspace: PropTypes.func,
  saveCustomField: PropTypes.func,
  resetCustomField: PropTypes.func,
  updateCustomRegex: PropTypes.func
};

export default InitForm;
