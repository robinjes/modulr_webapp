// Librairies
import React, { PropTypes } from 'react';
// Components
import InitForm from './InitForm';

class InitFormCtn extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <InitForm {...this.props} />
    );
  }
}

InitFormCtn.propTypes = {
  className: PropTypes.string
};

export default InitFormCtn;
