// Librairies
import React, { PropTypes } from 'react';
// Components
import Loader from 'components/atoms/Loader';
import Message from 'components/atoms/Message';
import Frame from './Frame';
// Styles
require('./styles');

// Render
class FrameCtn extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      photobooth: false,
      dropzone: true
    };

  }

  componentDidMount() {
    const { createFrame } = this.props,
      { docWidth, docHeight, vMat, hMat, moduleRef, deckRef, defaultColor } = this.props;

    createFrame(docWidth, docHeight, vMat, hMat, moduleRef, deckRef, defaultColor);

  }

  render() {

    const { isFetching, frame } = this.props;

    return (
      <div className="frameCtn">
        {frame.valid && isFetching &&
          <Loader>Meanwhile...</Loader>
        }
        {frame.valid && !isFetching &&
          <Frame
            {...this.props}
            photoboothEnabled={this.state.photobooth}
            dropzoneEnabled={this.state.dropzone}
          />
        }
        {!frame.valid && !isFetching &&
          <Message>Something went wrong, please try again from the beginning.</Message>
        }
      </div>
    );

  }
}

FrameCtn.propTypes = {
  isFetching: PropTypes.bool,
  docWidth: PropTypes.number,
  docHeight: PropTypes.number,
  vMat: PropTypes.number.isRequired,
  hMat: PropTypes.number.isRequired,
  moduleRef: PropTypes.object.isRequired,
  deckRef: PropTypes.object.isRequired,
  defaultColor: PropTypes.object.isRequired,
  frame: PropTypes.object.isRequired,
  createFrame: PropTypes.func
};

export default FrameCtn;
