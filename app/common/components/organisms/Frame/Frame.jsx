// Librairies
import React, { PropTypes } from 'react';
// Components
import PhotoBooth from 'components/molecules/PhotoBooth';
import DropZone from 'components/molecules/DropZone';
// Styles
require('./styles');
// Render

const Frame = (props) => {

  const { className, ratio, frame, moduleRef, children } = props,

    classCtn = className ? 'frame full-height ' + className : 'frame full-height',
    ratioFlt = parseFloat(ratio),
    frameWidth = ratioFlt * parseFloat(frame.width),
    frameHeight = ratioFlt * parseFloat(frame.height),
    moduleHeight = ratioFlt * parseFloat(moduleRef.height),
    innerFrameStyle = {
      width: String(frameWidth - (moduleHeight * 2)) + '%',
      height: String(frameHeight - (moduleHeight * 2)) + '%',
      top: String(((100 - frameHeight) / 2) + moduleHeight) + '%',
      left: String(((100 - frameWidth) / 2) + moduleHeight) + '%'
    };

  return (
    <div className={classCtn}>
      {children}
      <div className="innerFramePanel" style={innerFrameStyle}>
        {props.photoboothEnabled && !props.dropzoneEnabled &&
          <PhotoBooth />
        }
        {!props.photoboothEnabled && props.dropzoneEnabled &&
          <DropZone />
        }
      </div>
    </div>
  );

};

Frame.propTypes = {
  className: PropTypes.string,
  ratio: PropTypes.number,
  frame: PropTypes.object.isRequired,
  moduleRef: PropTypes.object.isRequired,
  photoboothEnabled: PropTypes.bool,
  dropzoneEnabled: PropTypes.bool,
  children: PropTypes.any
};

export default Frame;
