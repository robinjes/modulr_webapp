/* Librairies */
import thunkMiddleware from 'redux-thunk';
import { createStore, compose, applyMiddleware } from 'redux';
import { persistStore, autoRehydrate } from 'redux-persist';
/* Reducers */
import rootReducer from 'reducers';
// Utils
import * as api from 'utils/api';
// Initial state
import initialState from 'store/initialState';

const composeEnhancers =
    typeof window === 'object' &&
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ ?
    window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__({
      // Specify extension’s options like name, actionsBlacklist, actionsCreators, serialize...
    }) : compose,
  enhancers = composeEnhancers(
    applyMiddleware(
      thunkMiddleware
    ),
    autoRehydrate()
  ),
  frameStore = createStore(
    rootReducer,
    initialState,
    enhancers
  );

api.setStore(frameStore);

persistStore(frameStore);

export default frameStore;
