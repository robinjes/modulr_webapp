/* Private */
// Frame
export const prepareFrame = () => ({
    type: 'PREPARE_FRAME'
  }),
  // Add frame
  requestSaveFrame = () => ({
    type: 'REQUEST_SAVE_FRAME'
  }),
  frameSaved = (id, status) => ({
    type: 'FRAME_SAVED',
    id,
    status
  }),
  // Set frame algo
  requestUpdateFrameAlgo = () => ({
    type: 'REQUEST_UPDATE_FRAME_ALGO'
  }),
  updateFrameAlgo = () => ({
    type: 'UPDATE_FRAME_ALGO'
  }),
  frameAlgoUpdated = () => ({
    type: 'FRAME_ALGO_UPDATED'
  }),
  // Get frame
  requestFrame = () => ({
    type: 'REQUEST_FRAME'
  }),
  loadFrame = (res) => ({
    type: 'LOAD_FRAME',
    res
  }),
  receiveFrame = () => ({
    type: 'RECEIVE_FRAME'
  }),
  // Get public frame
  requestPublicFrame = () => ({
    type: 'REQUEST_PUBLIC_FRAME'
  }),
  loadPublicFrame = (res) => ({
    type: 'LOAD_PUBLIC_FRAME',
    res
  }),
  receivePublicFrame = () => ({
    type: 'RECEIVE_PUBLIC_FRAME'
  }),
  // Get all frames
  requestAllFrames = () => ({
    type: 'REQUEST_ALL_FRAMES'
  }),
  loadAllFrames = (res) => ({
    type: 'LOAD_ALL_FRAMES',
    res
  }),
  receiveAllFrames = () => ({
    type: 'RECEIVE_ALL_FRAMES'
  }),
  // Get all frames overviews
  requestAllFramesOverviews = () => ({
    type: 'REQUEST_ALL_FRAMES_OVERVIEWS'
  }),
  loadAllFramesOverviews = (res) => ({
    type: 'LOAD_ALL_FRAMES_OVERVIEWS',
    res
  }),
  receiveAllFramesOverviews = () => ({
    type: 'RECEIVE_ALL_FRAMES_OVERVIEWS'
  }),
  // Get all public frames
  requestAllPublicFrames = () => ({
    type: 'REQUEST_ALL_PUBLIC_FRAMES'
  }),
  loadAllPublicFrames = (res) => ({
    type: 'LOAD_ALL_PUBLIC_FRAMES',
    res
  }),
  receiveAllPublicFrames = () => ({
    type: 'RECEIVE_ALL_PUBLIC_FRAMES'
  }),
  // Get all public frames overviews
  requestAllPublicFramesOverviews = () => ({
    type: 'REQUEST_AL_PUBLICL_FRAMES_OVERVIEWS'
  }),
  loadAllPublicFramesOverviews = (res) => ({
    type: 'LOAD_ALL_PUBLIC_FRAMES_OVERVIEWS',
    res
  }),
  receiveAllPublicFramesOverviews = () => ({
    type: 'RECEIVE_ALL_PUBLIC_FRAMES_OVERVIEWS'
  }),
  // Frame 3D
  requestFrame3d = () => ({
    type: 'REQUEST_FRAME_3D'
  }),
  loadFrame3d = (res) => ({
    type: 'LOAD_FRAME_3D',
    res
  }),
  receiveFrame3d = () => ({
    type: 'RECEIVE_FRAME_3D'
  }),

  // Modules
  requestModules = () => ({
    type: 'REQUEST_MODULES'
  }),
  loadModules = (algo) => ({
    type: 'LOAD_MODULES',
    algo
  }),
  receiveModules = () => ({
    type: 'RECEIVE_MODULES'
  }),

  // Moldings
  requestMoldings = () => ({
    type: 'REQUEST_MOLDINGS'
  }),
  loadMoldings = (moldings) => ({
    type: 'LOAD_MOLDINGS',
    moldings
  }),
  receiveMoldings = () => ({
    type: 'RECEIVE_MOLDINGS'
  }),

  // Finitions
  requestFinitions = () => ({
    type: 'REQUEST_FINITIONS'
  }),
  loadFinitions = (finitions) => ({
    type: 'LOAD_FINITIONS',
    finitions
  }),
  receiveFinitions = () => ({
    type: 'RECEIVE_FINITIONS'
  }),

  // Login
  requestLogin = () => ({
    type: 'REQUEST_LOGIN'
  }),
  loggedIn = (response) => ({
    type: 'LOGGED_IN',
    response
  });
