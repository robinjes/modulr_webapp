// Librairies
import fetch from 'isomorphic-fetch';
// Utils
import * as api from 'utils/api';
// Actions
import * as pr from './privates';
import * as pu from './publics';

/* Thunk */

/**
* Generate Modules from Algo
*/
export const createModules = (algo) => (
    dispatch => {
      dispatch(pr.requestModules());
      dispatch(pr.loadModules(algo));
      return dispatch(pr.receiveModules());
    }
  ),
  /**
  * Generate Frame from Document Width & Height, Mat Width & Height,
  * Module & Deck reference and Color by default
  */
  createFrame = (docWidth, docHeight, vMat, hMat, moduleRef, deckRef, defaultColor) => (
    dispatch => {
      dispatch(pr.prepareFrame());
      dispatch(pu.buildFrame(docWidth, docHeight, vMat, hMat, moduleRef, deckRef, defaultColor));
      return dispatch(pr.receiveFrame());
    }
  ),
  /**
  * Save Frame from current user token, algo and public status
  */
  saveFrame = (token) => (
    (dispatch, getState) => {

      const { modules, frames } = getState(),
        items = modules.items;

      items.m = modules.m;
      /* eslint-disable one-var */
      const body = {
        id: frames.wip.id || '',
        name: frames.wip.name || '',
        token,
        items: JSON.stringify(items),
        public: frames.wip.isPublic || 0
      };
      /* eslint-enable one-var */

      /* Type checking */
      // if (typeof token !== 'string') {
      //   console.log(
      //     "Error: expected 'token' param type 'String', got '" +
      //     typeof token +
      //     "' instead"
      //   );
      //   return null;
      // }
      // if (typeof items !== 'object') {
      //   console.log(
      //     "Error: expected 'algo' param type 'Object', got '" +
      //     typeof items + "' instead"
      //   );
      //   return null;
      // }

      /* Request */
      dispatch(pr.requestSaveFrame());
      return api.post('http://127.0.0.1/modulr_webapp/app/server/api/add_frame.php', body)
        .then((res) => {
          if (res && res.id) {
            dispatch(pr.frameSaved(res.id, 'ok'));
          } else {
            dispatch(pr.frameSaved(null, 'ko'));
          }
        }
      );
    }
  ),
  /**
  * Get a Frame from ID & current user token
  */
  fetchFrame = (id, token) => (
    dispatch => {
      dispatch(pr.requestFrame());
      return fetch('http://127.0.0.1/modulr_webapp/app/server/api/get_frame.php', {
        method: 'post',
        headers: {
          'Content-type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        body: 'id=' + id + '&token=' + token
      })
      .then(response => response.json())
      .then(json =>
        dispatch(pr.loadFrame(json))
      )
      .then(
        pr.receiveFrame()
      );
    }
  ),
  /**
  * Get a Public Frame from ID
  */
  fetchPublicFrame = (id) => (
    dispatch => {
      dispatch(pr.requestPublicFrame());
      return fetch('http://127.0.0.1/modulr_webapp/app/server/api/get_frame_public.php', {
        method: 'post',
        headers: {
          'Content-type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        body: 'id=' + id
      })
      .then(response => response.json())
      .then(json =>
        dispatch(pr.loadPublicFrame(json))
      )
      .then(
        pr.receivePublicFrame()
      );
    }
  ),
  /**
  * Get all Frames overviews from ID & current user token
  */
  fetchAllFramesOverviews = (token) => (
    dispatch => {
      dispatch(pr.requestAllFramesOverviews());
      return fetch('http://127.0.0.1/modulr_webapp/app/server/api/get_frames_private_overviews.php', {
        method: 'post',
        headers: {
          'Content-type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        body: 'token=' + token
      })
      .then(response => response.json())
      .then(json =>
        dispatch(pr.loadAllFramesOverviews(json))
      )
      .then(
        pr.receiveAllFramesOverviews()
      );
    }
  ),
  /**
  * Get a 3D Frame from ID
  */
  fetchFrame3d = (id) => (
    dispatch => {
      dispatch(pr.requestFrame3d());
      return fetch('http://127.0.0.1/modulr_webapp/app/server/api/...', {
        method: 'post',
        headers: {
          'Content-type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        body: 'id=bar&lorem=ipsum'
      })
      .then(response => response.json())
      .then(json =>
        dispatch(pr.loadFrame3d(json))
      )
      .then(
        pr.receiveFrame3d()
      );
    }
  ),
  /**
  * Get all Frames for a user current user token
  */
  fetchAllFrames = (token) => (
    dispatch => {
      dispatch(pr.requestAllFrames());
      return fetch('http://127.0.0.1/modulr_webapp/app/server/api/get_frames_privates.php', {
        method: 'post',
        headers: {
          'Content-type': 'application/x-www-form-urlencoded; charset=UTF-8'
        },
        body: 'token=bar'
      })
      .then(response => response.json())
      .then(json =>
        dispatch(pr.loadAllFrames(json))
      )
      .then(
        pr.receiveAllFrames()
      );
    }
  ),
  /**
  * Get all Public Frames
  */
  fetchAllPublicFrames = () => (
    dispatch => {
      dispatch(pr.requestAllPublicFrames());
      return fetch('http://127.0.0.1/modulr_webapp/app/server/api/get_frames_publics.php')
      .then(response => response.json())
      .then(json =>
        dispatch(pr.loadAllPublicFrames(json))
      )
      .then(
        pr.receiveAllPublicFrames()
      );
    }
  ),
  /**
  * Get all Frames overviews from ID & current user token
  */
  fetchAllPublicFramesOverviews = () => (
    dispatch => {
      dispatch(pr.requestAllPublicFramesOverviews());
      return fetch('http://127.0.0.1/modulr_webapp/app/server/api/get_frames_public_overviews.php')
      .then(response => response.json())
      .then(json =>
        dispatch(pr.loadAllPublicFramesOverviews(json))
      )
      .then(
        pr.receiveAllPublicFramesOverviews()
      );
    }
  ),
  /**
  * Get all Finitions
  */
  fetchAllFinitions = () => (
    dispatch => {
      dispatch(pr.requestFinitions());
      return fetch('http://127.0.0.1/modulr_webapp/app/server/api/get_finitions.php')
      .then(response => response.json())
      .then(json =>
        dispatch(pr.loadFinitions(json))
      )
      .then(
        pr.receiveFinitions()
      );
    }
  ),
  /**
  * Get all Finitions
  */
  fetchAllMoldings = () => (
    dispatch => {
      dispatch(pr.requestMoldings());
      return fetch('http://127.0.0.1/modulr_webapp/app/server/api/get_moldings.php')
      .then(response => response.json())
      .then(json =>
        dispatch(pr.loadMoldings(json))
      )
      .then(
        pr.receiveMoldings()
      );
    }
  ),
  /**
  * Request Login
  */
  login = (usernameoremail, password) => (
    dispatch => {
      dispatch(pr.requestLogin());
      return fetch(`http://127.0.0.1/modulr_webapp/app/server/api/login.php?login=${usernameoremail}&password=${password}`)
      .then((res) => {
        dispatch(pr.loggedIn(res));
      })
      .catch((res) => {
        dispatch(pr.loggedIn(res));
      });
    }
  );
