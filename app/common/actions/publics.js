let moduleId = 0;

export const addModule = (size) => ({
    type: 'ADD_MODULE',
    id: moduleId++,
    size
  }),
  changeFrameColor = (colorRef) => ({
    type: 'CHANGE_FRAME_COLOR',
    colorRef
  }),
  changeModuleColor = (id, colorRef) => ({
    type: 'CHANGE_MODULE_COLOR',
    id,
    colorRef
  }),
  toggleModuleSelect = (id) => ({
    type: 'TOGGLE_MODULE_SELECT',
    id
  }),
  validateValue = (name, value, regex) => ({
    type: 'VALIDATE_VALUE',
    name,
    value,
    regex
  }),
  saveCustom = (name, value) => ({
    type: 'SAVE_CUSTOM',
    name,
    value
  }),
  resetCustom = (name) => ({
    type: 'RESET_CUSTOM',
    name
  }),
  updateRegex = (name, regexName, regexValue) => ({
    type: 'UPDATE_REGEX',
    name,
    regexName,
    regexValue
  }),
  updateCustomRegex = (name, customName, regexName, regexValue) => ({
    type: 'UPDATE_CUSTOM_REGEX',
    name,
    customName,
    regexName,
    regexValue
  }),
  resetWip = () => ({
    type: 'RESET_WIP'
  }),
  resetModules = () => ({
    type: 'RESET_MODULES'
  }),
  selectColor = (id) => ({
    type: 'SELECT_COLOR',
    id
  }),
  buildFrame = (docWidth, docHeight, vMat, hMat, moduleRef, deckRef, defaultColor) => ({
    type: 'BUILD_FRAME',
    docWidth,
    docHeight,
    vMat,
    hMat,
    moduleRef,
    deckRef,
    defaultColor
  }),
  updateFrameName = (name) => ({
    type: 'UPDATE_FRAME_NAME',
    name
  }),
  updateFrameStatus = (status) => ({
    type: 'UPDATE_FRAME_STATUS',
    status
  }),
  openModal = (modalType) => ({
    type: 'OPEN_MODAL',
    modalType
  }),
  closeModal = (modalType) => ({
    type: 'CLOSE_MODAL',
    modalType
  }),
  closeAllModals = () => ({
    type: 'CLOSE_ALL_MODALS'
  });
