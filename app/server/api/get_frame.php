<?php
// include core configuration
include_once '../config/core.php';

// include database connection
include_once '../config/database.php';

// product object
include_once '../objects/frame.php';

// class instance
$database = new Database();
$db = $database->getConnection();
$frame = new Frame($db);

// read all products
$frameid = $_GET['id'];
$token = $_GET['token'];
$results=$frame->getPrivate($token, $frameid);

// output in json format
echo $results;
?>
