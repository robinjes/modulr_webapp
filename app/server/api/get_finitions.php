<?php
// include core configuration
include_once '../config/core.php';

// include database connection
include_once '../config/database.php';

// product object
include_once '../objects/finition.php';

// class instance
$database = new Database();
$db = $database->getConnection();
$finition = new Finition($db);

// read all products
$results=$finition->getAll();

// output in json format
echo $results;
?>
