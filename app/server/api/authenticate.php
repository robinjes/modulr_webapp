<?php
// include core configuration
include_once '../config/core.php';

// include database connection
include_once '../config/database.php';

// class instance
$database = new Database();
$db = $database->getConnection();
$user = new User($db);

// get URL parameters
$jwtValue = '';

if(isset($_POST['jwtValue'])){
  $jwtValue = $_POST['jwtValue'];
} else {
  if (function_exists('http_response_code')) {
    http_response_code(401);
  } else {
    header('HTTP/1.1 401 Unauthorized', true, 401);
  }
}

$result = $user->authenticate($jwtValue);

echo $result;
