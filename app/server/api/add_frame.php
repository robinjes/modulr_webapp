<?php

// include temp header for dev.
include_once '../dev/temp_header.php';

// include core configuration
include_once '../config/core.php';

// include database connection
include_once '../config/database.php';

// product object
include_once '../objects/frame.php';

// class instance
$database = new Database();
$db = $database->getConnection();
$frame = new Frame($db);

// get URL parameters
$id = '';
$name = '';
$token = '';
$items = '';
$public = '';

if(isset($_POST['id'])){
    $id = $_POST['id'];
}
if(isset($_POST['name'])){
    $name = $_POST['name'];
}
if(isset($_POST['token'])){
    $token = $_POST['token'];
} else {
    if (function_exists('http_response_code')) {
      http_response_code(401);
    } else {
      header('HTTP/1.1 401 Unauthorized', true, 401);
    }
}
if(isset($_POST['items'])){
    $items = $_POST['items'];
}
if(isset($_POST['public'])){
    $public = $_POST['public'];
}

// $id = $_GET['id'];
// $name = $_GET['name'];
// $token = $_GET['token'];
// $items = $_GET['items'];
// $public = $_GET['public'];

// if((isset($token) && ($token!=null)) || (isset($token) && ($token!=null))) {
  if(isset($public) && (($public == 0) || ($public == 1))) {
    $results = $frame->create($id, $name, $token, $items, $public);
  } else {
    $results = $frame->create($id, $name, $token, $items, 0);
  }
  // output in json format
  echo $results;
// } else {
//   if (function_exists('http_response_code')) {
//     http_response_code(401);
//   } else {
//     header('HTTP/1.1 401 Unauthorized', true, 401);
//   }
// }

?>
