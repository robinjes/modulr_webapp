<?php
// include core configuration
include_once '../config/core.php';

// include database connection
include_once '../config/database.php';

// user object
include_once '../objects/user.php';

// class instance
$database = new Database();
$db = $database->getConnection();
$user = new User($db);

// get URL parameters
$usernameoremail = '';
$password = '';

if(isset($_GET['login'])){
  $usernameoremail = $_GET['login'];
} else {
  if (function_exists('http_response_code')) {
    http_response_code(401);
  } else {
    header('HTTP/1.1 401 Unauthorized', true, 401);
  }
}

if(isset($_GET['password'])){
    $password = $_GET['password'];
} else {
    if (function_exists('http_response_code')) {
      http_response_code(401);
    } else {
      header('HTTP/1.1 401 Unauthorized', true, 401);
    }
}

$result = $user->login($usernameoremail, $password);

echo $result;
