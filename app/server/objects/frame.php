<?php
class Frame {

    // database connection and table name
    private $conn;
    private $table_name = "frame";

    // object properties
    public $id;
    public $name;

    public function __construct($db){
        $this->conn = $db;
    }

    private function calculateBranch($branchArr) {
        $subAlgo = '';
        $distinctCounter = 0;
        $temp_colorRef = '';
        $max = sizeof($branchArr) - 1;
        if($max > 0) {
          // Loop through Array
          foreach ($branchArr as $key => $value) {
              // first time color, none before
              if(($temp_colorRef != $value["colorRef"]) && ($distinctCounter == 0)){
                  $temp_colorRef = $value["colorRef"];
                  $distinctCounter++;
              } else
              // first time color, stocked some before
              if(($temp_colorRef != $value["colorRef"]) && ($distinctCounter != 0)){
                  $subAlgo = $subAlgo . $distinctCounter . "x" . $temp_colorRef . "_";
                  $temp_colorRef = $value["colorRef"];
                  $distinctCounter = 1;
              } else
              // x time color
              if(($temp_colorRef == $value["colorRef"]) && ($distinctCounter != 0)){
                  $distinctCounter++;
              }
              // if last color, not different
              if($max == $key) {
                  $subAlgo = $subAlgo . $distinctCounter . "x" . $temp_colorRef;
                  $temp_colorRef = '';
                  $distinctCounter = 0;
              }
          }
        }
        return $subAlgo;
    }

    private function algo_compress($items) {
      $algo = '';

      // Convert JSON string to Array
      $globalArray = json_decode($items, true);

      $m = $globalArray["m"];
      $branch_tl = $globalArray["tl"];
      $branch_t = $globalArray["t"];
      $branch_tr = $globalArray["tr"];
      $branch_r = $globalArray["r"];
      $branch_br = $globalArray["br"];
      $branch_b = $globalArray["b"];
      $branch_bl = $globalArray["bl"];
      $branch_l = $globalArray["l"];

      $algo = $algo . 'm' . $m["x"] . '_' . $m["y"] . '.';
      $algo = $algo . $this->calculateBranch($branch_tl) . '_';
      $algo = $algo . $this->calculateBranch($branch_t) . '_';
      $algo = $algo . $this->calculateBranch($branch_tr) . '_';
      $algo = $algo . $this->calculateBranch($branch_r) . '_';
      $algo = $algo . $this->calculateBranch($branch_br) . '_';
      $algo = $algo . $this->calculateBranch($branch_b) . '_';
      $algo = $algo . $this->calculateBranch($branch_bl) . '_';
      $algo = $algo . $this->calculateBranch($branch_l);

      return $algo;   // m022_144.1xBLACK_2x05-899_...1x05-777_2xBLACK

      // <?php
      //
      //     function algo_uncompress($algo) {
      //         $output = '';
      //
      //         // $data = "foo:*:1023:1000::/home/foo:/bin/sh";
      //         // list($user, $pass, $uid, $gid, $gecos, $home, $shell) = explode(".", $data);
      //
      //         $explodedAlgo = explode(".", $algo);
      //         $explodedModQty = explode("_", $explodedAlgo[0]);
      //         $explodedMods = explode("_", $explodedAlgo[1]);
      //         echo $explodedModQty[0];    // 022
      //         echo $explodedModQty[1];    // 144
      //         echo $exploded[1];  // 1xBLACK_1xBLACK_2x05-899_1x05...
      //
      //         return $output;   // 022_144.1xBLACK_2x05-899_...1x05-777_2xBLACK
      //     }
      //
      //
      //         // JSON string
      //         $algo = '022_144.1xBLACK_1xBLACK_2x05-899_1x05-777_2xBLACK_1xBLACK_1xBLACK_2x05-899_1x05-777_2xBLACK_1xBLACK_1xBLACK_2x05-899_1x05-777_2xBLACK_1xBLACK_1xBLACK_2x05-899_1x05-777_2xBLACK';
      //
      //     echo algo_uncompress($algo);

    }

    /**
    * GET FRAME
    */
    public function getPrivate($token, $frameid){
      // QUERY
      $query = "SELECT frame.id, frame.algo, frame.modified, frame.thumb  FROM frame" .
                "WHERE frame.userId = (SELECT user.id FROM user WHERE token = '" .
                $token . "') AND frame.id = " . $frameid;
      // REQUEST
      $stmt = $this->conn->prepare($query);
      $stmt->execute();
      $frame=$stmt->fetchAll(PDO::FETCH_ASSOC);
      // RETURN
      return json_encode($frame);
    }

    /**
    * GET FRAME
    */
    public function getPublic($frameid){
      // QUERY
      $query = "SELECT frame.id, frame.algo, frame.modified, frame.thumb  FROM frame" .
                "WHERE frame.id = " . $frameid . " AND frame.public = 1";
      // REQUEST
      $stmt = $this->conn->prepare($query);
      $stmt->execute();
      $frame=$stmt->fetchAll(PDO::FETCH_ASSOC);
      // RETURN
      return json_encode($frame);
    }

    /**
    * GET ALL FRAMES
    */
    public function getAllPrivates($token){
      // QUERY
      $query = "SELECT frame.id, frame.algo, frame.modified, frame.thumb  FROM frame" .
                "WHERE frame.userId = (SELECT user.id FROM user WHERE token = '" .
                $token . "')";
      // REQUEST
      $stmt = $this->conn->prepare($query);
      $stmt->execute();
      $frame=$stmt->fetchAll(PDO::FETCH_ASSOC);
      // RETURN
      return json_encode($frame);
    }

    /**
    * GET ALL PUBLIC FRAMES
    */
    public function getAllPublics(){
      // QUERY
      $query = "SELECT frame.id, frame.algo, frame.thumb FROM frame" .
                "WHERE frame.public = 1";
      // REQUEST
      $stmt = $this->conn->prepare($query);
      $stmt->execute();
      $frame=$stmt->fetchAll(PDO::FETCH_ASSOC);
      // RETURN
      return json_encode($frame);
    }

    /**
    * GET ALL FRAMES COLUMNS
    */
    public function getAllPrivatesColumn($column, $token){
      // QUERY
      $query = "SELECT " . $column . " FROM frame" .
      "WHERE (frame.userId = (SELECT user.id FROM user WHERE token = '" .
      $token . "'))";
      // REQUEST
      $stmt = $this->conn->prepare($query);
      $stmt->execute();
      $frame=$stmt->fetchAll(PDO::FETCH_ASSOC);
      // RETURN
      return json_encode($frame);
    }

    /**
    * GET ALL PUBLIC FRAMES COLUMNS
    */
    public function getAllPublicsColumn($column){
      // QUERY
      $query = "SELECT " . $column . " FROM frame" .
                "WHERE frame.public = 1";
      // REQUEST
      $stmt = $this->conn->prepare($query);
      $stmt->execute();
      $frame=$stmt->fetchAll(PDO::FETCH_ASSOC);
      // RETURN
      return json_encode($frame);
    }

    /**
    * CREATE NEW FRAME
    */
    public function create($id, $name, $token, $items, $public) {
      $algo_compressed = $this->algo_compress($items);

      // check if UPDATE or CREATE
      if(!isset($id)||($id == '')) {
        // CREATE
        try{
          // DATA
          $modified = date('Y-m-d H:i:s');
          $id = uniqid(); // 13 characters unique id
          // QUERIES
          $query_1 = "SELECT id FROM user WHERE token = '" . $token . "'";
          $query_2 = "INSERT INTO frame (id, name, algo, modified, public, userId) VALUES ('" .
                    $id . "','" .
                    $name . "','" .
                    $algo_compressed . "','" .
                    $modified . "'," .
                    $public . "," .
                    "(SELECT id FROM user WHERE token = '" . $token . "')" .
                    ")";

          // REQUESTS
          $stmt_1 = $this->conn->prepare($query_1);
          $stmt_2 = $this->conn->prepare($query_2);

          if($stmt_1->execute()){
            if($stmt_1->rowCount() > 0 ) {
              // User exists
              if($stmt_2->execute()){
                return json_encode(array('id' => $id, 'created' => $modified));
              } else {
                $arr = $stmt_2->errorInfo();
                return json_encode(array('id' => null, 'message' => $arr[2]));
              }
            } else {
                if (function_exists('http_response_code')) {
                  http_response_code(401);
                } else {
                  header('HTTP/1.1 401 Unauthorized', true, 401);
                }
            }
          } else {
            $arr = $stmt_1->errorInfo();
            return json_encode(array('id' => null, 'message' => $arr[2]));
          }
        }
        // ERROR
        catch(PDOException $exception){
          die('ERROR: ' . $exception->getMessage());
        }
      } else {
        // UPDATE
        return $this->updateFrame($items, $name, $id, $token, $public);
      }

    }

    /**
    * UPDATE FRAME VALUE
    */
    public function updateFrame($items, $newName, $frameid, $token, $public){
      $algo_compressed = $this->algo_compress($items);
      try {
        // DATA
        $modified = date('Y-m-d H:i:s');
        // QUERIES
        $query_1 = "SELECT * FROM frame " .
                  "WHERE id = '" . $frameid . "'";
        $query_2 = "UPDATE frame SET " .
                  "name = '" . $newName . "'," .
                  "algo = '" . $algo_compressed . "'," .
                  "modified = '" . $modified . "', " .
                  "public = " . $public . " " .
                  "WHERE id = '" . $frameid . "' AND " .
                  "userId = (SELECT id FROM user WHERE token = '" . $token . "')";
        // REQUESTS
        $stmt_1 = $this->conn->prepare($query_1);
        $stmt_2 = $this->conn->prepare($query_2);

        if($stmt_1->execute()){
          if($stmt_1->rowCount() > 0 ) {
            // Frame exists
            if($stmt_2->execute()) {
              return json_encode(array('id' => $frameid, 'modified' => $modified));
            } else {
              if (function_exists('http_response_code')) {
                http_response_code(400);
              } else {
                header('HTTP/1.1 400 Bad Request', true, 400);
              }
              $arr = $stmt_2->errorInfo();
              return json_encode(array('id' => $frameid, 'modified' => null, 'message' => $query_2));
            }
          }
          // Frame doesnt exist
          return $this->create('', $newName, $token, $items, $public);
          // return json_encode(array('id' => $frameid, 'modified' => null, 'message' => 'No frame to update, create a new one instead.'));

        } else {
          // if (function_exists('http_response_code')) {
          //   http_response_code(400);
          // } else {
          //   header('HTTP/1.1 400 Bad Request', true, 400);
          // }
          $arr = $stmt_1->errorInfo();
          return json_encode(array('id' => $frameid, 'modified' => null, 'message' => $arr[2]));
        }
      }
      // ERROR
      catch(PDOException $exception){
        die('ERROR: ' . $exception->getMessage());
      }

    }

}
?>
