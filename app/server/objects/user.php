<?php
require_once '../../../vendor/autoload.php';

use \Firebase\JWT\JWT;

define('SECRET_KEY','Your-Secret-Key');
define('ALGORITHM','HS512');

class User {

    // database connection and table name
    private $conn;
    private $table_name = "user";

    // object properties
    public $user;

    public function __construct($db){
        $this->conn = $db;
    }

    /**
    * GET ALL FINITIONS
    */
    public function login($usernameoremail, $password) {

        $statement = $this->conn->prepare("select * from login where username = :usernameoremail" );
        $statement->execute(array(':usernameoremail' => $usernameoremail));
        $row = $statement->fetchAll(PDO::FETCH_ASSOC);
        $hashedPwd = password_hash($password, PASSWORD_BCRYPT);

        if(count($row)>0) {
            if(password_verify($row[0]['pwd'], $hashedPwd)) {
                $tokenId    = base64_encode(mcrypt_create_iv(32));
                $issuedAt   = time();
                $notBefore  = $issuedAt + 10;  // Adding 10 seconds
                $expire     = $notBefore + 7200; // Adding 60 seconds
                $serverName = 'http://modulr.olfraime.fr'; // Set domain name

                /* Create the token as an array */
                $data = [
                  'iat'  => $issuedAt,         // Issued at: time when the token was generated
                  'jti'  => $tokenId,          // Json Token Id: an unique identifier for the token
                  'iss'  => $serverName,       // Issuer
                  'nbf'  => $notBefore,        // Not before
                  'exp'  => $expire,           // Expire
                  'data' => [                  // Data related to the logged user you can set your required data
                      'username'   => $row[0]['username'], // username from the users table
                      'email' => $row[0]['email'], //  email from the users table
                  ]
                ];

                $secretKey = base64_decode(SECRET_KEY);
                // Here we will transform this array into JWT:
                $jwt = JWT::encode(
                  $data, //Data to be encoded in the JWT
                  $secretKey, // The signing key
                  ALGORITHM
                );

                $unencodedArray = ['jwt' => $jwt];
                echo  "{'status' : 'success','res':".json_encode($unencodedArray)."}";
            } else {
                echo  "{'status' : 'error','msg':'Invalid password : " . $row[0]['pwd'] . " vs. " . $hashedPwd . "'}";
            }
        } else {
          echo  "{'status' : 'error','msg':'Invalid email or username : " . $usernameoremail . "'}";
        }



    //   // REQUEST
    //   $stmt_1 = $this->conn->prepare($query_1);
    //
    //   if($stmt_1->execute()) {
    //     $user = $stmt_1->fetch(PDO::FETCH_ASSOC);
    //     if($user) {
    //       // user exists
    //       $userId = $user['id'];
    //       $query_2 = "select * from access_token where user_id = '" . $userId . "'";
    //       $stmt_2 = $this->conn->prepare($query_2);
    //       if($stmt_2->execute()) {
    //         $access_token = $stmt_2->fetch(PDO::FETCH_ASSOC);
    //         if($access_token) {
    //           // token exists
    //           return json_encode(array('token' => $access_token['token']));
    //         } else {
    //           // token DOESNT exist
    //           return json_encode(array('message' => 'no token available'));
    //         }
    //       } else {
    //         $arr = $stmt_1->errorInfo();
    //         return json_encode(array('message' => $arr[2]));
    //       }
    //     } else {
    //       // user DOESNT exist
    //       if (function_exists('http_response_code')) {
    //         http_response_code(401);
    //       } else {
    //         header('HTTP/1.1 401 Unauthorized', true, 401);
    //       }
    //     }
    //   } else {
    //     // error
    //     $arr = $stmt_1->errorInfo();
    //     return json_encode(array('message' => $arr[2]));
    //   }
    //   // RETURN
    //   return json_encode($finitions);
  }

  public function authenticate($jwtValue) {
    try {
      $secretKey = base64_decode(SECRET_KEY);
      $DecodedDataArray = JWT::decode($jwtValue, $secretKey, array(ALGORITHM));

      echo  "{'status' : 'success' ,'data':" . json_encode($DecodedDataArray) . " }";
      die();

    } catch (Exception $e) {
      echo "{'status' : 'fail' ,'msg':'Unauthorized'}";
      die();
    }
  }
}
?>
