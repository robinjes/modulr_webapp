<?php
class Molding {

    // database connection and table name
    private $conn;
    private $table_name = "molding";

    // object properties
    public $id;
    public $name;

    public function __construct($db){
        $this->conn = $db;
    }

    /**
    * GET ALL MOLDINGS
    */
    public function getAll(){
      // QUERY
      $query = "SELECT molding.id, molding.name, finition.shape FROM " . $table_name;
      // REQUEST
      $stmt = $this->conn->prepare($query);
      $stmt->execute();
      $moldings = $stmt->fetchAll(PDO::FETCH_ASSOC);
      // RETURN
      return json_encode($moldings);
    }

}
?>
