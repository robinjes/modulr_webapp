<?php
class Finition {

    // database connection and table name
    private $conn;
    private $table_name = "finitions";

    // object properties
    public $id;
    public $name;

    public function __construct($db){
        $this->conn = $db;
    }

    /**
    * GET ALL FINITIONS
    */
    public function getAll(){
      // QUERY
      $query = "SELECT finition.id, finition.name, finition.red, finition.green, finition.blue, finition.texture  FROM " . $table_name;
      // REQUEST
      $stmt = $this->conn->prepare($query);
      $stmt->execute();
      $finitions = $stmt->fetchAll(PDO::FETCH_ASSOC);
      // RETURN
      return json_encode($finitions);
    }

}
?>
