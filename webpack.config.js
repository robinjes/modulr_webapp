const webpack = require('webpack');
const path = require('path');
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const nodeEnv = process.env.NODE_ENV || 'development';
const isProd = nodeEnv === 'production';

const sourcePath = path.join(__dirname, 'app');
const staticsPath = path.join(__dirname, 'dist');

const extractCSS = new ExtractTextPlugin({ filename: 'style.css', disable: false, allChunks: true });

const plugins = [
  new webpack.optimize.CommonsChunkPlugin({
    name: 'vendor',
    minChunks: Infinity,
    filename: 'vendor.bundle.js'
  }),
  new webpack.DefinePlugin({
    'process.env': { NODE_ENV: JSON.stringify(nodeEnv) }
  }),
  new HtmlWebpackPlugin({
    template: sourcePath + '/index.html',
    production: isProd,
    inject: true,
  }),
];

const jsEntry = [
  'app/client/index'
];

if (isProd) {
  plugins.push(
    new webpack.LoaderOptionsPlugin({
      minimize: true,
      debug: false
    }),
    // Minimize all JavaScript output of chunks
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false,
        screw_ie8: true,
        conditionals: true,
        unused: true,
        comparisons: true,
        sequences: true,
        dead_code: true,
        evaluate: true,
        if_return: true,
        join_vars: true,
      },
      output: {
        comments: false
      },
    }),
    extractCSS
  );

  jsEntry.unshift(
    'webpack-dev-server/client?http://localhost:8080',
    'webpack/hot/only-dev-server'
  );
} else {
  plugins.push(
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin()
  );
}

module.exports = {
  devtool: isProd ? 'source-map' : 'cheap-module-source-map',
  context: sourcePath,
  entry: {
    js: jsEntry,
    vendor: [
      'react',
      'react-dom',
      'react-router',
      'react-redux',
      'redux',
      'redux-persist',
      'reselect',
      'immutable',
      'three-js'
    ]
  },
  output: {
    path: staticsPath,
    filename: 'scripts/[name].[chunkhash].js',
    chunkFilename: '[chunkhash].js',
    publicPath: '/',
  },
  module: {
    rules: [
      {
        // ESLint
        test: /\.(js|jsx)$/,
        include: sourcePath,
        exclude: sourcePath + '/common/static/',
        enforce: 'pre',
        use: {
          loader: 'eslint-loader',
          options: {
            configFile: '.eslintrc',
            include: 'common'
          }
        }
      },
      /* {
        test: /\.html$/,
        use: {
          loader: 'file-loader',
          query: {
            name: '[name].[ext]'
          }
        }
      },*/
      {
        // PostCSS
        test: /\.css/,
        use: [
          {
            loader: 'postcss-loader',
            options: {
              plugins: function () {
                return [
                  require('autoprefixer')
                ];
              }
            }
          }
        ]
      },
      {
        test: /\.less$/,
        use: isProd ?
          extractCSS.extract({
            fallbackLoader: 'style-loader',
            loader: ['css-loader?sourceMap', 'postcss-loader', 'less-loader'],
          }) :
          ['style-loader', 'css-loader', 'postcss-loader', 'less-loader']
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          {
            loader: 'babel-loader',
            query: {
              cacheDirectory: true
            }
          }
        ]
      },
      {
        test: /\.(gif|png|jpg|jpeg\ttf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
        use: 'file-loader'
      }
    ],
  },
  resolve: {
    extensions: ['.json', '.js', '.jsx', '.less'],
    modules: [
      sourcePath,
      'node_modules'
    ],
    alias: {
      app: sourcePath,
      common: 'app/common',
      components: 'common/components',
      constants: 'common/constants',
      assets: 'common/assets',
      actions: 'common/actions',
      reducers: 'common/reducers',
      selectors: 'common/selectors',
      utils: 'common/utils',
      store: 'common/store',
      stylesheets: 'common/stylesheets',
      server: 'app/server'
    }
  },
  plugins,
  // DevServer
  devServer: {
    contentBase: './client',
    historyApiFallback: true,
    port: 8080,
    hot: true,
    compress: isProd,
    stats: { colors: true },
  }
};
