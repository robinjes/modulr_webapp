# ModulR Webapp

ModulR Design web application.

More information about the product on [https://www.olfraime.com/modulr](https://www.olfraime.com/modulr/modulr)

## Install
```php composer.phar install```

**Development mode** :
```npm run dev```

**Production build** :
```npm run prod```

# Tools/Versions

### Front-side
- **ES6**
- **React** as UI framework.
- **Redux** as state manager.
- **React router** as routes manager.
- **Redux Thunk Middleware** as Redux asynchronous actions middleware.
- **Redux Persist** as persisting & rehydrating Redux store tool.
- **LESS** as CSS pre-processor.

### Server-side
- **PHP 7.0**
- **PHP JWT** as  encoding & decoding JSON Web Tokens PHP tool.
- **MySQL 5.7** as a relational database management system.

### Development
- **Babel and plugins** as transpiler.
- **Webpack** as modules bundler.
- **Eslint and AirBnb config** as code checker.

# Ressources

- [Project Guideline](https://github.com/wearehive/project-guidelines)
- [Towards A Simpler React Folder Structure](http://andrewcallahan.com/towards-a-simpler-react-folder-structure/)
- [Code Splitting for React Router with ES6 Imports](http://moduscreate.com/code-splitting-for-react-router-with-es6-imports/)
- [Loading Images](http://survivejs.com/webpack/loading-assets/loading-images/)
- [JSON-SERVER](https://github.com/typicode/json-server)
- [Redux Persist](https://github.com/rt2zz/redux-persist)
- [The Beginner’s Guide to three.js](http://blog.teamtreehouse.com/the-beginners-guide-to-three-js)
- [Implementing a smart Login Modal with Redux, reselect and ReactJS](https://medium.com/@dorsha/implement-login-modal-with-redux-reselect-and-reactjs-668c468bcbe3)
- [Securing React Redux Apps With JWT Tokens](https://medium.com/@rajaraodv/securing-react-redux-apps-with-jwt-tokens-fcfe81356ea0)
- [PHP-JWT](https://github.com/firebase/php-jwt)




